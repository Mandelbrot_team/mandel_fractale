package gravitySimulator;

// @author Raphaël

public class CelestialBody
{
    String name;
    double mass;
    Vector2D position, speed;
    
    public CelestialBody(String name, double mass, Vector2D position, Vector2D speed)
    {
        this.name = name;
        this.mass = mass;
        this.position = position;
        this.speed = speed;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public double getMass()
    {
        return this.mass;
    }
    
    public Vector2D getPosition()
    {
        return this.position;
    }
    
    public Vector2D getSpeed()
    {
        return this.speed;
    }
    
    public double getX()
    {
        return this.position.getXVal();
    }
    
    public double getY()
    {
        return this.position.getYVal();
    }
    
    public double getVx()
    {
        return this.speed.getXVal();
    }
    
    public double getVy()
    {
        return this.speed.getYVal();
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setMass(double mass)
    {
        this.mass = mass;
    }
    
    public void setPosition(Vector2D position)
    {
        this.position = position;
    }
    
    public void setSpeed(Vector2D speed)
    {
        this.speed = speed;
    }
    
    public void setX(double x)
    {
        this.position.setXval(x);
    }
    
    public void setY(double y)
    {
        this.position.setYVal(y);
    }
    
    public void setVx(double Vx)
    {
        this.speed.setXval(Vx);
    }
    
    public void setVy(double Vy)
    {
        this.speed.setYVal(Vy);
    }
}
