package gravitySimulator;

// @author Raphaël

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GravitySimulator extends JPanel implements ActionListener, WindowListener
{
    public static double G = 6.67384E-11;
    
    protected JPanel ButtonBar;
        protected JButton resetSimulationButton;
        protected JButton startSimulationButton;
        protected JButton stopSimulationButton;
    protected JPanel GraphicPanel;
    protected JPanel StatusBar;
    
    protected ArrayList<CelestialBody> bodies;
    protected ArrayList<Vector2D> path;
    protected boolean activeSimulation;
    
    public GravitySimulator()
    {
        super(new BorderLayout());
        
        this.ButtonBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
            this.resetSimulationButton = new JButton("Réinitialiser");
            this.startSimulationButton = new JButton("Commencer Simulation");
            this.stopSimulationButton = new JButton("Arrêter Simulation");
        this.GraphicPanel = new GraphicPanel(this);
        this.StatusBar = new JPanel();
        
        this.add(this.ButtonBar, BorderLayout.NORTH);
            this.ButtonBar.add(this.resetSimulationButton);
            this.ButtonBar.add(this.startSimulationButton);
            this.ButtonBar.add(this.stopSimulationButton);
        this.add(this.GraphicPanel, BorderLayout.CENTER);
        this.add(this.StatusBar, BorderLayout.SOUTH);
        
        this.resetSimulationButton.addActionListener(this);
        this.startSimulationButton.addActionListener(this);
        this.stopSimulationButton.addActionListener(this);
        
        this.bodies = new ArrayList();
        this.setInitialValues();
    }
    
    public static void main(String[] args)
    {
        GravitySimulator content = new GravitySimulator();
        JFrame frame = new JFrame("Gravity Simulator 2014");
        frame.setContentPane(content);
        frame.addWindowListener((WindowListener)content);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == this.resetSimulationButton)
        {
            this.activeSimulation = false;
            this.setInitialValues();
            this.GraphicPanel.repaint();
        }
        else if(e.getSource() == this.startSimulationButton)
        {
            this.activeSimulation = true;
            (new Thread(new MovementProcess(this))).start();
        }
        else if(e.getSource() == this.stopSimulationButton)
        {
            this.activeSimulation = false;
        }
    }
    
    @Override
    public void windowOpened(WindowEvent e)
    {
        
    }
    
    @Override
    public void windowClosing(WindowEvent e)
    {
        
    }
    
    @Override
    public void windowClosed(WindowEvent e)
    {
        
    }
    
    @Override
    public void windowIconified(WindowEvent e)
    {
        
    }
    
    @Override
    public void windowDeiconified(WindowEvent e)
    {
        
    }
    
    @Override
    public void windowActivated(WindowEvent e)
    {
        
    }
    
    @Override
    public void windowDeactivated(WindowEvent e)
    {
        
    }
    
    public void setInitialValues()
    {
        this.bodies.clear();
        this.bodies.add(new CelestialBody("Soleil", 1.9891E30, new Vector2D(0, 0), new Vector2D(0, 0)));
        this.bodies.add(new CelestialBody("Mercure", 3.3020E23, new Vector2D(0, 4.6001E10), new Vector2D(5.8990E4, 0)));
        this.bodies.add(new CelestialBody("Vénus", 4.8685E24, new Vector2D(0, 1.0748E11), new Vector2D(3.5265E4, 0)));
        this.bodies.add(new CelestialBody("Terre", 5.9736E24,  new Vector2D(0, 1.4710E11), new Vector2D(3.1145E4, 0)));
        this.bodies.add(new CelestialBody("Mars", 6.4185E23, new Vector2D(0, 2.0626E11), new Vector2D(2.6521E4, 0)));
        this.bodies.add(new CelestialBody("Jupiter", 1.8986E27, new Vector2D(0, 7.4038E11), new Vector2D(1.3714E4, 0)));
        this.bodies.add(new CelestialBody("Saturne", 5.6846E26, new Vector2D(0, 1.3498E12), new Vector2D(1.0182E4, 0)));
        this.bodies.add(new CelestialBody("Uranus", 8.6832E25, new Vector2D(0, 2.7350E12), new Vector2D(7.1285E3, 0)));
        this.bodies.add(new CelestialBody("Neptune", 1.0243E26, new Vector2D(0, 4.4597E12), new Vector2D(5.4802E3, 0)));
    }
    
    public void iterateMovements(double DT, int n)
    {
        for(int k = 0; k < n; k++)
        {
            for(int i = 0; i < this.bodies.size(); i++)
            {
                CelestialBody intB = this.bodies.get(i); // corps considere / interieur du systeme
                Vector2D globF = new Vector2D(0, 0); // force globale exercee sur le systeme
                
                for(int j = 0; j < this.bodies.size(); j++)
                {
                    if(j != i)
                    {
                        CelestialBody extB = this.bodies.get(j); // corps exterieur / exterieur du systeme
                        Vector2D relPos = new Vector2D(extB.getX()-intB.getX(), extB.getY()-intB.getY()), // position relative du corps exterieur par rapport au corps considere
                                 extF = new Vector2D(); // force exercee par le corps exterieur sur le corps considere
                        
                        extF.setPolCoord(G*intB.getMass()*extB.getMass()/relPos.getSquaredModulus(),
                            relPos.getArgument());
                        
                        globF.add(extF);
                    }
                }
                
                Vector2D A = new Vector2D(globF.getXVal()/intB.getMass(),
                    globF.getYVal()/intB.getMass()); // acceleration
                
                intB.setX(A.getXVal()*DT*DT/2+intB.getVx()*DT+intB.getX()); // position
                intB.setY(A.getYVal()*DT*DT/2+intB.getVy()*DT+intB.getY());
                
                intB.setVx(A.getXVal()*DT+intB.getVx()); // vitesse
                intB.setVy(A.getYVal()*DT+intB.getVy());
            }
        }
    }
    
    public class MovementProcess implements Runnable
    {
        GravitySimulator parent;
        
        public MovementProcess(GravitySimulator parent)
        {
            this.parent = parent;
        }
        
        @Override
        public void run()
        {
            while(this.parent.activeSimulation)
            {
                this.parent.iterateMovements(5000, 1);
                this.parent.GraphicPanel.repaint();
                
                try
                {
                    Thread.sleep(1);
                }
                catch(InterruptedException ex)
                {
                    Logger.getLogger(GravitySimulator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    }
}
