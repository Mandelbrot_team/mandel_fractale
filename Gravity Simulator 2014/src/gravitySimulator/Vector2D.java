package gravitySimulator;

// @author Raphaël

public class Vector2D
{
    double xVal, yVal;
    
    public Vector2D()
    {
        this.xVal = 0;
        this.yVal = 0;
    }
    
    public Vector2D(double xVal, double yVal)
    {
        this.xVal = xVal;
        this.yVal = yVal;
    }
    
    public double getXVal()
    {
        return this.xVal;
    }
    
    public double getYVal()
    {
        return this.yVal;
    }
    
    public double getModulus()
    {
        return Math.sqrt(this.xVal*this.xVal+this.yVal*this.yVal);
    }
    
    public double getSquaredModulus()
    {
        return this.xVal*this.xVal+this.yVal*this.yVal;
    }
    
    public double getArgument()
    {
        return Math.atan2(this.yVal, this.xVal);
    }
    
    public void setXval(double xVal)
    {
        this.xVal = xVal;
    }
    
    public void setYVal(double yVal)
    {
        this.yVal = yVal;
    }
    
    public void addDx(double Dx)
    {
        this.xVal += Dx;
    }
    
    public void addDy(double Dy)
    {
        this.yVal += Dy;
    }
    
    public void addDxDy(double Dx, double Dy)
    {
        this.xVal += Dx;
        this.yVal += Dy;
    }
    
    public void setCartCoord(double xVal, double yVal)
    {
        this.xVal = xVal;
        this.yVal = yVal;
    }
    
    public void setPolCoord(double modulus, double argument)
    {
        this.xVal = modulus*Math.cos(argument);
        this.yVal = modulus*Math.sin(argument);
    }
    
    public void add(Vector2D vect)
    {
        this.xVal += vect.xVal;
        this.yVal += vect.yVal;
    }
    
    public void substract(Vector2D vect)
    {
        this.xVal -= vect.xVal;
        this.yVal -= vect.yVal;
    }
    
    public double scalar(Vector2D vect)
    {
        return this.xVal*vect.xVal+this.yVal*vect.yVal;
    }
}
