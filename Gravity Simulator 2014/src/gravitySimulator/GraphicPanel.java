package gravitySimulator;

// @author Raphaël

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GraphicPanel extends JPanel implements MouseMotionListener, MouseWheelListener
{
    public static Image[] images = new Image[9];
    
    private GravitySimulator parent;
    private double zoom;
    
    public GraphicPanel(GravitySimulator parent)
    {
        this.parent = parent;
        this.zoom = 9.5;
        
        this.addMouseWheelListener(this);
        
        this.setBackground(Color.BLACK);
        
        try
        {
            images[0] = ImageIO.read(getClass().getResource("res/Soleil.png"));
            images[1] = ImageIO.read(getClass().getResource("res/Mercure.png"));
            images[2] = ImageIO.read(getClass().getResource("res/Vénus.png"));
            images[3] = ImageIO.read(getClass().getResource("res/Terre.png"));
            images[4] = ImageIO.read(getClass().getResource("res/Mars.png"));
            images[5] = ImageIO.read(getClass().getResource("res/Jupiter.png"));
            images[6] = ImageIO.read(getClass().getResource("res/Saturne.png"));
            images[7] = ImageIO.read(getClass().getResource("res/Uranus.png"));
            images[8] = ImageIO.read(getClass().getResource("res/Neptune.png"));
        }
        catch (IOException e)
        {
            System.out.println("erreur !");
        }
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        for(int i = 0; i < this.parent.bodies.size(); i++)
        {
            g.drawImage(this.images[i], (int)(this.parent.bodies.get(i).getX()/Math.pow(10, this.zoom))+this.getWidth()/2-this.images[i].getWidth(null)/2,
                       (int)(this.parent.bodies.get(i).getY()/Math.pow(10, this.zoom))+this.getHeight()/2-this.images[i].getHeight(null)/2, null);
        }
    }
    
    @Override
    public void mouseDragged(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        if(e.getWheelRotation() > 0)
        {
            this.zoom += 0.1;
            this.repaint();
        }
        else if(e.getWheelRotation() < 0)
        {
            this.zoom -= 0.1;
            this.repaint();
        }
    }
    
    public static void drawImage(Graphics g, Color c)
    {
        
    }
}
