package fractal.generator;

// @author Raphaël

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class Sprite
{
    private BufferedImage image;
    private IntPoint origin;
    private int backgroundRGB;
    
    public Sprite(BufferedImage image, IntPoint origin)
    {
        ArrayList<Integer> edgeColors = new ArrayList();
        int imageWidth, imageHeight, currentColor, dominantColor = 0, currentColorOccurrence, maxOccurrence = 0;
        
        this.image = image;
        this.origin = origin;
        imageWidth = image.getWidth();
        imageHeight = image.getHeight();

        for(int i = 0; i < imageWidth; i++)
        {
            edgeColors.add(new Integer(image.getRGB(i, 0)));
            edgeColors.add(new Integer(image.getRGB(i, imageHeight-1)));
        }

        for(int j = 1; j < imageHeight-1; j++)
        {
            edgeColors.add(new Integer(image.getRGB(0, j)));
            edgeColors.add(new Integer(image.getRGB(imageWidth-1, j)));
        }

        while(!edgeColors.isEmpty())
        {
            int i = 0;

            currentColor = edgeColors.get(i).intValue();
            currentColorOccurrence = 1;
            edgeColors.remove(i);

            while(i < edgeColors.size())
            {
                if(edgeColors.get(i).intValue() == currentColor)
                {
                    currentColorOccurrence++;
                    edgeColors.remove(i);
                }
                else
                {
                    i++;
                }
            }

            if(currentColorOccurrence > maxOccurrence)
            {
                dominantColor = currentColor;
                maxOccurrence = currentColorOccurrence;
            }
        }

        this.backgroundRGB = dominantColor;
    }
    
    public BufferedImage getImage()
    {
        return this.image;
    }
    
    public int getWidth()
    {
        return this.image.getWidth();
    }
    
    public int getHeight()
    {
        return this.image.getHeight();
    }
    
    public IntPoint getOrigin()
    {
        return this.origin;
    }
    
    public int getBackgroundRGB()
    {
        return this.backgroundRGB;
    }
    
    public void setImage(BufferedImage image)
    {
        this.image = image;
    }
    
    public void setOrigin(IntPoint origin)
    {
        this.origin = origin;
    }
    
    public void setBackgroundRGB(int backgroundRGB)
    {
        this.backgroundRGB = backgroundRGB;
    }
    
    public boolean isBackground(int x, int y)
    {
        return this.image.getRGB(x, y) == this.backgroundRGB;
    }
    
    public void printOnGraphics(Graphics g, int x, int y)
    {
        int currentColor;
        
        x -= this.origin.getAbs();
        y -= this.origin.getOrd();
        
        for(int i = 0; i < this.image.getWidth(); i++)
        {
            for(int j = 0; j < this.image.getHeight(); j++)
            {
                if((currentColor = this.image.getRGB(i, j)) != this.backgroundRGB)
                {
                    g.setColor(new Color(currentColor));
                    g.drawLine(x+i, y+j, x+i, y+j);
                }
            }
        }
    }
    
    public void printOnGraphics(Graphics g, IntPoint screenPos)
    {
        this.printOnGraphics(g, screenPos.getAbs(), screenPos.getOrd());
    }
}
