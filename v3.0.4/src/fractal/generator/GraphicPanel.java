package fractal.generator;



// @author Raphaël

import static fractal.generator.Layout.forcePlaceDouble;
import static fractal.generator.FractalGenerator.MBy1;
import static fractal.generator.FractalGenerator.MBy2;
import static fractal.generator.FractalGenerator.getExponent;
import static fractal.generator.FractalGenerator.getMantissa;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.color.ColorSpace;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class GraphicPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener
{
    private FractalGenerator parent;
    private IntPoint dynamicPos;
    private BufferedImage pinImage;
    private Sprite pinSprite;
    private Pin developpedPin;
    
    public GraphicPanel(FractalGenerator parent)
    {
        this.parent = parent;
        
        try
        {   
            this.pinImage = ImageIO.read(new File(OS.IMAGES_FOLDER.getAbsolutePath()+"\\punaise.png"));
            this.pinSprite = new Sprite(this.pinImage, new IntPoint(this.pinImage.getWidth()/2-1, this.pinImage.getHeight()));
        }
        catch (IOException ex)
        {
            Logger.getLogger(GraphicPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.developpedPin = null;
        
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {  
        super.paintComponent(g);
        
        if(this.parent.exploImage != null)
        {
            g.drawImage(this.parent.exploImage,
                    this.getWidth()/2-this.parent.exploImage.getWidth()/2,
                    this.getHeight()/2-this.parent.exploImage.getHeight()/2, null);
            
            for(int i = 0; i < this.parent.pins.size(); i++)
            {
                Pin currentPin = this.parent.pins.get(i);
                
                if(this.parent.exploGC.getZoom() <= currentPin.getZoom())
                {
                    this.pinSprite.printOnGraphics(g, currentPin.getScreenPos());
                }
            }
            
            if(this.dynamicPos != null)
            {
                if(this.parent.holdingAPin)
                {
                    this.pinSprite.printOnGraphics(g, this.dynamicPos.getAbs(),
                            this.dynamicPos.getOrd());
                }
                else if(this.developpedPin != null)
                {
                    Color backgroundColor = new Color(255, 255, 255, 200);
                    int boxX = this.developpedPin.getScreenPos().getAbs()-100,
                        boxY = this.developpedPin.getScreenPos().getOrd()-this.developpedPin.getSprite().getHeight()-120;
                    char[] nameLabel = this.developpedPin.getName().toCharArray(),
                           xPosLabel = ("x : "+Double.toString(this.developpedPin.getRealPos().getAbs())).toCharArray(),
                           yPosLabel = ("y : "+Double.toString(this.developpedPin.getRealPos().getOrd())).toCharArray(),
                           zoomLabel = ("zoom : "+forcePlaceDouble(getMantissa(this.developpedPin.getZoom()), 12, false)
                            +"x10^"+Integer.toString(getExponent(this.developpedPin.getZoom()))).toCharArray();
                    
                    g.setColor(backgroundColor);
                    /*RoundRectangle2D rect = new RoundRectangle2D.Double(this.developpedPin.getScreenPos().getAbs()-100,
                            this.developpedPin.getScreenPos().getOrd()-this.developpedPin.getSprite().getHeight()-120,
                            200, 100, 20, 20);*/
                    
                    g.fillRoundRect(boxX, boxY, 200, 100, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawChars(nameLabel, 0, nameLabel.length, boxX+10, boxY+20);
                    g.drawChars(xPosLabel, 0, xPosLabel.length, boxX+10, boxY+40);
                    g.drawChars(yPosLabel, 0, yPosLabel.length, boxX+10, boxY+60);
                    g.drawChars(zoomLabel, 0, zoomLabel.length, boxX+10, boxY+80);
                }
                else
                {
                    double DX = this.parent.exploGC.getWidth()/this.parent.exploZoomMultiplicator,
                           DY = this.parent.exploGC.getHeight()/this.parent.exploZoomMultiplicator;

                    g.setColor(Color.WHITE);
                    g.drawRect(this.dynamicPos.getAbs()-(int)(DX/2),
                            this.dynamicPos.getOrd()-(int)(DY/2), (int)DX, (int)DY);
                }
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        
    }
    
    @Override
    public void mousePressed(MouseEvent e)
    {
        if(this.parent.exploImage != null)
        {
            int pressX = e.getX()-this.getWidth()/2+this.parent.exploGC.getWidth()/2, // position cliquee sur l'image
                pressY = e.getY()-this.getHeight()/2+this.parent.exploGC.getHeight()/2;
            
            if(e.getButton() == MouseEvent.BUTTON1)
            {
                if(this.parent.holdingAPin) // on place une épingle
                {
                    double resolution = this.parent.exploResolution(),
                           x = this.parent.exploGC.getCenter().getAbs() + (e.getX()-this.getWidth()/2) / resolution,
                           y = this.parent.exploGC.getCenter().getOrd() + (e.getY()-this.getHeight()/2) / resolution;
                    Point pressRealPosition = new Point(x, y);
                    
                    this.parent.pins.add(new Pin("Point de vue " + (this.parent.pins.size()+1), this.pinSprite, pressRealPosition, this.parent.exploGC.getZoom()));
                    this.calculatePinsScreenPosition();
                    this.parent.holdingAPin = false;
                    this.repaint();
                }
                else // on zoom
                {
                    int portionWidth = (int)(this.parent.exploGC.getWidth()/this.parent.exploZoomMultiplicator), // dimensions du cadre de zoom
                        portionHeight = (int)(this.parent.exploGC.getHeight()/this.parent.exploZoomMultiplicator);

                    FractalGenerator.translate(this.parent.exploGC, pressX, pressY);
                    this.parent.exploGMatrix = this.parent.exploGMatrix.getPortion(pressX-portionWidth/2, pressY-portionHeight/2, portionWidth, portionHeight);

                    for(int i = 0; i < 7; i++)
                    {
                        this.parent.exploGMatrix.trim();
                    }

                    this.parent.exploGMatrix.stretch(this.parent.exploGC.getWidth(), this.parent.exploGC.getHeight());
                    this.parent.exploGC.setZoom(this.parent.exploGC.getZoom()*this.parent.exploZoomMultiplicator);
                    this.parent.exploImage = this.parent.createMandelbrotFractal(this.parent.exploGC, this.parent.exploGMatrix, 10);
                    this.calculatePinsScreenPosition();
                    this.repaint();
                }
            }
            else if(e.getButton() == MouseEvent.BUTTON3)
            {   
                FractalGenerator.translate(this.parent.exploGC, pressX, pressY);
                this.parent.exploGC.setZoom(this.parent.exploGC.getZoom()/this.parent.exploZoomMultiplicator);
                this.parent.exploGMatrix = new GenerationMatrix(this.parent.exploGC.getWidth(), this.parent.exploGC.getHeight());
                this.parent.exploImage = this.parent.createMandelbrotFractal(this.parent.exploGC, this.parent.exploGMatrix, 10);
                this.calculatePinsScreenPosition();
                this.repaint();
            }

            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            System.out.println(this.parent.information(this.parent.exploGC));
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseDragged(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e)
    {
        Pin currentPin = null;
        int i = this.parent.pins.size()-1, XPosOnSprite, YPosOnSprite;
        
        this.dynamicPos = new IntPoint(e.getX(), e.getY());
        
        while(i > -1)
        {
            currentPin = this.parent.pins.get(i);
            
            if(this.parent.exploGC.getZoom() <= currentPin.getZoom())
            {
                if(currentPin.checkCollision(e.getX(), e.getY()))
                {
                    break;
                }
            }
            
            i--;
        }
        
        if(i > -1)
        {
            this.developpedPin = currentPin;
        }
        else
        {
            this.developpedPin = null;
        }
        
        this.repaint();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        if(e.getWheelRotation() > 0 && this.parent.exploZoomMultiplicator > 1.0)
        {
            this.parent.exploZoomMultiplicator--;
            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            this.repaint();
        }
        else if(e.getWheelRotation() < 0 && this.parent.exploZoomMultiplicator < 100.0)
        {
            this.parent.exploZoomMultiplicator++;
            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            this.repaint();
        }
    }
    
    public void calculatePinsScreenPosition()
    {
        double resolution = this.parent.exploResolution();
        int pinX, pinY;
        
        for(int i = 0; i < this.parent.pins.size(); i++)
        {
            pinX = (int)((this.parent.pins.get(i).getRealPos().getAbs()-this.parent.exploGC.getCenter().getAbs()) * resolution) + this.getWidth()/2;
            pinY = (int)((this.parent.pins.get(i).getRealPos().getOrd()-this.parent.exploGC.getCenter().getOrd()) * resolution) + this.getHeight()/2;
            this.parent.pins.get(i).setScreenPos(new IntPoint(pinX, pinY));
        }
    }
}
