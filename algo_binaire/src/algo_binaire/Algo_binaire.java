/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package algo_binaire;

/**
 *
 * @author Raphaël
 */
public class Algo_binaire
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        String bin = "";
        int i = 0, N = 56;
        
        while(N != 0)
        {
            bin = (N%2 == 1 ? "1" : "0") + bin;
            N /= 2;
        }
        
        System.out.println(bin);
        System.out.println(Integer.MIN_VALUE);
        System.out.println(Integer.MAX_VALUE);
    }
    
}
