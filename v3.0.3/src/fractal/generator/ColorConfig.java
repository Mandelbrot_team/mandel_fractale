/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import java.awt.image.BufferedImage;
import java.nio.Buffer;

/**
 *
 * @author Grégoire
 */
public class ColorConfig {
    private BufferedImage exmapleImage;
    private String function ;
    private String name;

    public ColorConfig(BufferedImage exmapleImage, String function, String name) {
        this.exmapleImage = exmapleImage;
        this.function = function;
        this.name = name;
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

            
    
    
    public BufferedImage getExmapleImage() {
        return exmapleImage;
    }

    public void setExmapleImage(BufferedImage exmapleImage) {
        this.exmapleImage = exmapleImage;
    }

    public String getFunction() {
        return function;
    }

    public void setFuntion(String funtion) {
        this.function = funtion;
    }
    
   
}
