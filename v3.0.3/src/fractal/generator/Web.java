/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import javax.swing.JApplet;
import javax.swing.JFrame;

/**
 *
 * @author Grégoire
 */
public class Web extends JApplet{
    public static void main(String[] args) {
        Web w = new Web();
        w.add(new FractalGenerator(new JFrame()));
        w.init();
        w.setVisible(true);
                
    }
}
