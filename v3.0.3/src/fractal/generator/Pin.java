package fractal.generator;

// @author Raphaël

public class Pin
{
    private String name;
    private Point position;
    private double zoom;
    
    public Pin(String name, Point position, double zoom)
    {
        this.name = name;
        this.position = position;
        this.zoom = zoom;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public Point getPosition()
    {
        return this.position;
    }
    
    public double getZoom()
    {
        return this.zoom;
    }
}
