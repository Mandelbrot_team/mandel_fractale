__declspec(dllexport) int iterate(double z_r,double z_i,double c_r,double c_i,int maxIteration){
double tmp;
int i=0;
while(z_r*z_r + z_i*z_i < 4 && i <maxIteration){
	tmp = z_r;
	z_r = z_r*z_r - z_i*z_i + c_r;
	z_i = 2*z_i*tmp + c_i;
	i++;
}
return i;
}
__declspec(dllexport) void simpleMandelbrot(int width, int height,double resolution,double x0,double y0,int maxIteration,int *res){
	double c_r,c_i,z_r,z_i;
	int i;
	for(int x=0;x<width;x++){
		for(int y=0;y<height;y++){
					c_r = x / resolution + x0;
                    c_i = y / resolution + y0;
                    z_r = 0;
                    z_i = 0;
                    i = iterate(z_r,z_i,c_r,c_i,maxIteration);
					res[y*width+x]=i;

		}
	}
}
