package fractal.generator;



// @author Raphaël

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;

public class FractalGenerator extends JPanel implements ActionListener
{
    protected JPanel buttonBar;
        protected JButton screenshotButton;
        protected JButton explorationButton;
        //protected JButton vonKochInitializeButton;
        protected JButton simulationButton;
    protected JScrollPane scrollerPanel;
    protected GraphicPanel graphicPanel;
    protected JLabel stateLabel;
    protected Progress prog = new Progress();
    
    protected BufferedImage exploImage; 
    protected GenerationMatrix exploGMatrix;
    public static String screenshotDirectory;
    protected GenerationConfig exploGC, simGC;
    protected RenderConfig simRC;
    protected double exploZoomMultiplicator;
    
    public static enum ExplorationProperties{explo_size, explo_zoom,
    explo_center, explo_maxIteration, explo_allProperties}
    public static enum SimulationProperties{simu_size, simu_zoom, simu_center,
    simu_maxIteration, simu_videoFrameDirectory, simu_videoDirectory,
    simu_imagePer10PowerNbr, simu_imageNbr, simu_threadNbr, simu_allProperties}
    
    public static final double MBx1 = -2.1, MBx2 = 0.6, MBy1 = -1.2, MBy2 = 1.2; // bornes de l'ensemble de Mandelbrot, sans zoom
    public static final int HDWidth = 1920, HDHeight = 1080;
    
    protected ColorCalcGenerator calcGen = new ColorCalcGenerator();
    protected Method getRgb =calcGen.loadMethod();
    
    protected JFrame parent;
    public FractalGenerator(JFrame parent )
    {
        
        super(new BorderLayout());
        
        
        this.parent=parent;
        this.buttonBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
            this.screenshotButton = new JButton("Capture d'écran");
            this.explorationButton = new JButton("Exploration");
            //this.vonKochInitializeButton = new JButton("Initialiser Von Koch");
            this.simulationButton = new JButton("Commencer Simulation");
        this.graphicPanel = new GraphicPanel(this);
        this.stateLabel = new JLabel();
        this.scrollerPanel=new JScrollPane();
        this.scrollerPanel.setLayout(new ScrollPaneLayout());
        this.add(this.buttonBar, BorderLayout.NORTH);
            this.buttonBar.add(this.screenshotButton);
            this.buttonBar.add(this.explorationButton);
            //this.buttonBar.add(this.vonKochInitializeButton);
            this.buttonBar.add(this.simulationButton);
        this.add(this.graphicPanel, BorderLayout.CENTER);
        this.add(this.stateLabel,BorderLayout.SOUTH);
        
        this.screenshotButton.addActionListener(this);
        this.explorationButton.addActionListener(this);
        //this.vonKochInitializeButton.addActionListener(this);
        this.simulationButton.addActionListener(this);
        
        this.screenshotDirectory = "C:\\Users\\Raphaël\\Pictures\\fractales\\screenshots\\";
        this.exploGC = new GenerationConfig();
        this.simGC = new GenerationConfig();
        this.simRC = new RenderConfig();
        this.exploZoomMultiplicator = 10;
        
        this.screenshotButton.setEnabled(false);
     
        OS.checkEssentialFolder();
        //OS.clearTempFile();
        OS.createAppDataFolder();
        if (!OS.checkInitFile()) {
            SetInitFile set = new SetInitFile();
            set.setVisible(true);
            set.setLocationRelativeTo(this);
            set.setAlwaysOnTop(true);
        }
        else{
            setDir(OS.readInitFile());
        }
        
    }
    public static void setDir(String [] args){
        FractalGenerator.screenshotDirectory=args[0]; // assuming right entry
    }
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("Fractal Generator 2014");
        FractalGenerator mainPanel = new FractalGenerator(frame);
        
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == this.screenshotButton && this.exploImage != null)
        {
            File file;
            int i = 0;
                
            do
            {
                file = new File(screenshotDirectory + "screenshot_" + completeWithZeros(i, 6) + ".png");
                i++;
            } while(file.exists());
            
            saveImage(this.exploImage, file);
        }
        
        else if(e.getSource() == this.explorationButton)
        {
            ExplorationController exploCont = new ExplorationController(this);
            exploCont.setVisible(true);
        }
        /*else if(e.getSource() == this.vonKochInitializeButton)
        {
            this.exploImage = createVonKochFractal();
            this.repaint();
        }*/
        else if(e.getSource() == this.simulationButton)
        {
            SimulationController simCont = new SimulationController(this);
            simCont.setVisible(true);
        }
    }
    
    public void startExploration()
    {
        this.exploGMatrix = new GenerationMatrix(this.exploGC.getWidth(), this.exploGC.getHeight());
        this.exploImage = createMandelbrotFractal(this.exploGC, this.exploGMatrix);
        this.stateLabel.setText(this.information(this.exploGC));
        System.out.println(this.information(this.exploGC));
        this.screenshotButton.setEnabled(true);
        this.repaint();
    }
    
    public void startSimulation()
    {
        ArrayList<Thread> threads = new ArrayList();
        
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                prog.setVisible(true);
            }
        });
        
        this.simulationButton.setEnabled(false);
        
        for(int i = 0; i < this.simRC.getThreadNbr(); i++)
        {
            GenerationConfig clonedGC = null;
            
            try
            {
                clonedGC = this.simGC.clone();
            }
            catch(CloneNotSupportedException ex)
            {
                Logger.getLogger(FractalGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            threads.add(new Thread(new ImageRender(clonedGC, this.simRC, i)));
            threads.get(i).start();
        }
    }
    
    public static BufferedImage createMandelbrotFractal(GenerationConfig gC, GenerationMatrix gMatrix)
    {
        BufferedImage img = new BufferedImage(gC.getWidth(), gC.getHeight(), BufferedImage.TYPE_INT_RGB);
        double DX = gC.getWidth(), // largeur image en pixel
               DY = gC.getHeight(), // hauteur image en pixel
               Dy = (MBy2-MBy1)/gC.getZoom(), // hauteur nouveau point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur nouveau point de vue en unite du plan
               resolution = DY/Dy, // nombre de pixels par unité du plan
               x0 = gC.getCenter().getAbs()-Dx/2, // nouvelles bornes avec zoom
               y0 = gC.getCenter().getOrd()-Dy/2,
               c_r, c_i, z_r, z_i, tmp; // valeurs reelles et imaginaires pour le calcul des termes de la suite
//        int i;
//        
//        for(int x = 0; x < gC.getWidth(); x++)
//        {
//            for(int y = 0; y < gC.getHeight(); y++)
//            {
//                if(gMatrix.getMatrix()[x][y])
//                {
//                    img.setRGB(x, y, 0);
//                }
//                else
//                {
//                    c_r = x / resolution + x0;
//                    c_i = y / resolution + y0;
//                    z_r = 0;
//                    z_i = 0;
//                    i = 0;
//
//                    do
//                    {
//                        tmp = z_r;
//                        z_r = z_r*z_r - z_i*z_i + c_r;
//                        z_i = 2*z_i*tmp + c_i;
//                        i++;
//                    } while(z_r*z_r + z_i*z_i < 4 && i < gC.getMaxIteration());
//
//                    if(i == gC.getMaxIteration())
//                    {
//                        img.setRGB(x, y, 0);
//                        gMatrix.getMatrix()[x][y] = true;
//                    }
//                    else
//                    {
//                        //config 1
//                        /*int red =(int)((i/(gC.getMaxIteration()+0.0)*2000))%255;
//                        int green = (int)((i/(gC.getMaxIteration()+0.0)*5000))%255;
//                        int blue=(int)((i/(gC.getMaxIteration()+0.0)*200))%255;
//                        img.setRGB(x, y, new Color(red, green, blue).getRGB());*/
//                        
//                        //config 2
//                        //img.setRGB(x, y, 2*i*i+5*i);
//                        
//                        //config 3
//                        //img.setRGB(x, y, new Color(i%256, i*i%256, i*i*i%256).getRGB()); // attention au depassement de l'int
//
//                        int [] rgb =gC.getRGB(i,z_r); // max iteration is given by this in GenerationCOnfig
//                        img.setRGB(x, y, new Color(rgb[0]%255,rgb[1]%255, rgb[2]%255).getRGB()); // attention au depassement de l'int
//
//                        
//                        //config 4
//                        //img.setRGB(x, y, i);
//                        
//                        //config 5
//                        
//                    }
//                }
//            }
//        }
        int iMat[][] = WrapperManager.getI(gC.getWidth(), gC.getHeight(), resolution, x0, y0, gC.getMaxIteration());
        for (int i = 0; i < iMat.length; i++) {
            for (int j = 0; j < iMat[i].length; j++) {
                if (iMat[i][j]==gC.getMaxIteration()) {
                    img.setRGB(i, j, 0);
                }
                else{
                    int [] rgb =gC.getRGB(i,15); // max iteration is given by this in GenerationCOnfig
                    img.setRGB(i, j, new Color(rgb[0]%255,rgb[1]%255, rgb[2]%255).getRGB()); // attention au depassement de l'int
                }
            }
        }
        return img;
    }
    
    public BufferedImage createMandelbrotFractal(GenerationConfig gC, GenerationMatrix gMatrix, int threadNbr)
    {
        BufferedImage img = new BufferedImage(gC.getWidth(), gC.getHeight(), BufferedImage.TYPE_INT_RGB);
        ArrayList<Thread> threads = new ArrayList();
        
        for(int i = 0; i < threadNbr; i++)
        {
            int startLine = gC.getHeight()*i/threadNbr,
                endLine = gC.getHeight()*(i+1)/threadNbr;
            
            threads.add(new Thread(new ParcelGenerator(this, img, gC, gMatrix, startLine, endLine)));
            threads.get(i).start();
        }
        
        /*for(int i = 0; i < threadNbr; i++)
        {
            while(threads.get(i).isAlive());
        }*/
        
        return img;
    }
    
    /*public void createVonKochFractal()
    {
        ArrayList<Point> points = new ArrayList();
        
        /*points.add(new Point(300, 200));
        points.add(new Point(900, 200));
        points.add(new Point(600, 600));
        
        points.add(new Point(300, 400));
        points.add(new Point(1000, 400));

        for(int i = 0; i < 8; i++)
        {
            applyIteration(points);
        }
        
        int[] xValues = new int[points.size()],
              yValues = new int[points.size()];
        
        for(int i = 0; i < points.size(); i++)
        {
            xValues[i] = (int)Math.round(points.get(i).getAbs());
            yValues[i] = (int)Math.round(points.get(i).getOrd());
        }

        this.image = new BufferedImage(this.graphicPanel.getWidth(), this.graphicPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = this.image.createGraphics();
        g.fillPolygon(xValues, yValues, points.size());
    }*/
    
    /*public static void applyIteration(ArrayList<Point> points)
    {
        double x1, y1, x2, y2, x3, y3;
        double newX1, newY1, xm, ym, newX2, newY2, newX3, newY3;
        double crossProductZ;
        
        for(int i = 0; i < points.size(); i += 4)
        {
            x1 = points.get(i).getAbs();
            y1 = points.get(i).getOrd();
            x2 = points.get((i+1)%points.size()).getAbs();
            y2 = points.get((i+1)%points.size()).getOrd();
            x3 = points.get((i+2)%points.size()).getAbs();
            y3 = points.get((i+2)%points.size()).getOrd();
            
            newX1 = 2*x1/3 + x2/3;
            newY1 = 2*y1/3 + y2/3;
            
            xm = (x1+x2)/2;
            ym = (y1+y2)/2;
            
            /*crossProductZ = (x2-x1)*(y3-y1)-(y2-y1)*(x3-x1); // composante sur Z du produit vectoriel
            
            if(crossProductZ < 0) // le point 3 est a droite du segment
            {
                newX2 = xm - Math.sqrt(3)*(y2-y1)/6;
                newY2 = ym + Math.sqrt(3)*(x2-x1)/6;
            /*}
            else
            {
                newX2 = xm + Math.sqrt(3)*(y2-y1)/6;
                newY2 = ym - Math.sqrt(3)*(x2-x1)/6;
            }
            
            newX3 = x1/3 + 2*x2/3;
            newY3 = y1/3 + 2*y2/3;
            
            points.add(i+1, new Point(newX1, newY1));
            points.add(i+2, new Point(newX2, newY2));
            points.add(i+3, new Point(newX3, newY3));
        }
    }*/
   
    public  void saveImage(BufferedImage img, File file)
    {
        
        if (new File(screenshotDirectory).exists()) {
             try
        {
            ImageIO.write(img, "PNG", file);
            Toast.makeText(this.parent,"Saved at "+file.getPath(),Toast.Style.SUCCESS).display();
        }
        catch (Exception ex)
        {
             Toast.makeText(this.parent,"Fail to saved at "+file.getPath(),Toast.Style.ERROR).display();
        }
        }
        else{
             Toast.makeText(this.parent,"This directory doesn't exist"+screenshotDirectory,Toast.Style.ERROR).display();
        }
       
    }
    
    public static void translate(GenerationConfig gC, int XC, int YC) // XC et YC sont les cordonnees sur l'image en pixels de la nouvelle position du centre
    {
        double DX = gC.getWidth(),
               DY = gC.getHeight(),
               Dy = (MBy2-MBy1)/gC.getZoom(), // hauteur point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur point de vue en unite du plan
               DxC = (XC/DX-0.5)*Dx, // deplacement centre de vue sur x en unite du plan
               DyC = (YC/DY-0.5)*Dy; // deplacement centre de vue sur y en unite du plan

        gC.getCenter().setCoordinates(gC.getCenter().getAbs()+DxC, gC.getCenter().getOrd()+DyC);
    }
    
    protected class ImageRender implements Runnable
    {
        private GenerationConfig gC;
        private RenderConfig rC;
        private GenerationMatrix gMatrix;
        private int threadIndex;
            
        public ImageRender(GenerationConfig gC, RenderConfig rC, int threadIndex)
        {
            this.gC = gC;
            this.rC = rC;
            this.gMatrix = new GenerationMatrix(this.gC.getWidth(), this.gC.getHeight()); // attention si on change la taille !!
            this.threadIndex = threadIndex;
        }
        
        @Override
        public void run()
        {
            double lastZoom;
            int startIndex = this.rC.getImageNbr()*this.threadIndex/this.rC.getThreadNbr(),
                i = startIndex,
                j = 1,
                endIndex = this.rC.getImageNbr()*(this.threadIndex+1)/this.rC.getThreadNbr();
            
            while (i < endIndex && prog.isGo())
            {
                lastZoom = this.gC.getZoom();
                this.gC.setZoom(Math.pow(10, (i/this.rC.getImagePer10PowerNbr())));
                
                if(i > startIndex)
                {
                    int portionWidth = (int)(this.gC.getWidth()/this.gC.getZoom()*lastZoom), // dimensions du cadre de zoom
                        portionHeight = (int)(this.gC.getHeight()/this.gC.getZoom()*lastZoom);

                    this.gMatrix = this.gMatrix.getPortion(this.gC.getWidth()/2-portionWidth/2, this.gC.getHeight()/2-portionHeight/2, portionWidth, portionHeight);

                    for(int k = 0; k < 4; k++)
                    {
                        this.gMatrix.trim();
                    }

                    this.gMatrix.stretch(this.gC.getWidth(), this.gC.getHeight());
                }
                
                BufferedImage img = createMandelbrotFractal(this.gC, this.gMatrix);
                saveImage(img, new File(this.rC.getVideoFrameDirectory()
                        + "frac_frame_"
                        + completeWithZeros(i, length(this.rC.getImageNbr()))
                        + ".png"));
                System.out.println("Thread n°" + this.threadIndex + " : img n°" + i + " generee");
                i++;
                j++;
                prog.getjProgressBar1().setValue(prog.getjProgressBar1().getValue()+(int)((float)j/this.rC.getImageNbr()*100));
            }
        }
    }
    
    public class ParcelGenerator implements Runnable
    {
        FractalGenerator parent;
        BufferedImage img;
        GenerationConfig gC;
        GenerationMatrix gMatrix;
        int startLine, endLine;
        
        public ParcelGenerator(FractalGenerator parent, BufferedImage img, GenerationConfig gC, GenerationMatrix gMatrix, int startLine, int endLine)
        {
            this.parent = parent;
            this.img = img;
            this.gC = gC;
            this.gMatrix = gMatrix;
            this.startLine = startLine;
            this.endLine = endLine;
        }
        
        public void run()
        {
            double DX = this.gC.getWidth(), // largeur image en pixel
               DY = this.gC.getHeight(), // hauteur image en pixel
               Dy = (MBy2-MBy1)/this.gC.getZoom(), // hauteur nouveau point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur nouveau point de vue en unite du plan
               resolution = DY/Dy, // nombre de pixels par unité du plan
               x0 = this.gC.getCenter().getAbs()-Dx/2, // nouvelles bornes avec zoom
               y0 = this.gC.getCenter().getOrd()-Dy/2,
               c_r, c_i, z_r, z_i, tmp; // valeurs reelles et imaginaires pour le calcul des termes de la suite
//            int i;
            
//            for(int y = this.startLine; y < this.endLine; y++)
//            {
//                for(int x = 0; x < this.gC.getWidth(); x++)
//                {
//                    if(this.gMatrix.getMatrix()[x][y])
//                    {
//                        this.img.setRGB(x, y, 0);
//                    }
//                    else
//                    {
//                        c_r = x / resolution + x0;
//                        c_i = y / resolution + y0;
//                        z_r = 0;
//                        z_i = 0;
//                        i = 0;
//
//                        do
//                        {
//                            tmp = z_r;
//                            z_r = z_r*z_r - z_i*z_i + c_r;
//                            z_i = 2*z_i*tmp + c_i;
//                            i++;
//                        } while(z_r*z_r + z_i*z_i < 4 && i < this.gC.getMaxIteration());
//
//                        if(i == this.gC.getMaxIteration())
//                        {
//                            this.img.setRGB(x, y, 0);
//                            this.gMatrix.getMatrix()[x][y] = true;
//                        }
//                        else
//                        {
//                            //config 1
//                            /*int red =(int)((i/(gC.getMaxIteration()+0.0)*2000))%255;
//                            int green = (int)((i/(gC.getMaxIteration()+0.0)*5000))%255;
//                            int blue=(int)((i/(gC.getMaxIteration()+0.0)*200))%255;
//                            this.img.setRGB(x, y, new Color(red, green, blue).getRGB());*/
//
//                            //config 2
//                            //this.img.setRGB(x, y, 2*i*i+5*i);
//
//                            //config 3
//                            //this.img.setRGB(x, y, new Color(i%256, i*i%256, i*i*i%256).getRGB()); // attention au depassement de l'int
//                            
//                            //config 4
//                            //this.img.setRGB(x, y, i);
//                            
//                            //config 5
////                            if(i < 256)
////                                img.setRGB(x, y, new Color(i, 0, 0).getRGB());
////                            else if(i < 512)
////                                img.setRGB(x, y, new Color(255, i-256, 0).getRGB());
////                            else
////                                img.setRGB(x, y, new Color(255, 255, i-512).getRGB());
//                            int [] rgb =gC.getRGB(i,z_r); // max iteration is given by this in GenerationCOnfig
//                        img.setRGB(x, y, new Color(rgb[0]%255,rgb[1]%255, rgb[2]%255).getRGB()); // attention au depassement de l'int
//                        }
//                    }
//                }
//                
//                this.parent.repaint();
//            }
//            
//            
             int iMat[][] = WrapperManager.getI(gC.getWidth(), gC.getHeight(), resolution, x0, y0, gC.getMaxIteration());
            for (int i = 0; i < iMat.length; i++) {
                for (int j = 0; j < iMat[i].length; j++) {
                    
                        int [] rgb =gC.getRGB(iMat[i][j],1005); // max iteration is given by this in GenerationCOnfig
                        img.setRGB(i, j, (new Color(rgb[0]%255,rgb[1]%255, rgb[2]%255)).getRGB()%16777216); // attention au depassement de l'int
                   
                   this.parent.repaint(); 
                }
                 
            }
        }
    }
    
    public static String completeWithZeros(int n, int totalSize)
    {
        String chain = Integer.toString(n);
        int chainLength = chain.length();
        
        for(int i = 0; i < totalSize-chainLength; i++)
        {
            chain = '0' + chain;
        }
        
        return chain;
    }
    
    public static int length(int n)
    {
        return Integer.toString(n).length();
    }
    
    public String information(GenerationConfig gC)
    {
        return "pos : (" + gC.getCenter().getAbs()
                + ", " + gC.getCenter().getOrd()
                + ") - zoom : " + gC.getZoom()
                + " - multiplicateur : " + (int)this.exploZoomMultiplicator;
    }
    
    public void setExplorationDefaultValues(ExplorationProperties exploProp)
    {
        boolean allProperties = exploProp == ExplorationProperties.explo_allProperties;
        
        switch(exploProp)
        {
            case explo_allProperties :
            case explo_size :
                this.exploGC.setWidth(this.graphicPanel.getWidth());
                this.exploGC.setHeight(this.graphicPanel.getHeight());
                if(!allProperties)break;
            case explo_zoom :
                this.exploGC.setZoom(1);
                if(!allProperties)break;
            case explo_center :
                this.exploGC.getCenter().setCoordinates(-0.75, 0);
                if(!allProperties)break;
            case explo_maxIteration :
                this.exploGC.setMaxIteration(500);
        }
    }
    
    public void setSimulationDefaultValues(SimulationProperties simuProp)
    {
        boolean allProperties = simuProp == SimulationProperties.simu_allProperties;
        
        switch(simuProp)
        {
            case simu_allProperties :
            case simu_size :
                this.simGC.setWidth(HDWidth);
                this.simGC.setHeight(HDHeight);
                if(!allProperties)break;
            case simu_zoom :
                this.simGC.setZoom(1);
                if(!allProperties)break;
            case simu_center :
                this.simGC.getCenter().setCoordinates(-0.75, 0);
                if(!allProperties)break;
            case simu_maxIteration :
                this.simGC.setMaxIteration(2000);
                if(!allProperties)break;
            case simu_videoFrameDirectory :
                this.simRC.setVideoFrameDirectory("D:\\");
                if(!allProperties)break;
            case simu_videoDirectory :
                this.simRC.setVideoDirectory("D:\\");
                if(!allProperties)break;
            case simu_imagePer10PowerNbr :
                this.simRC.setImagePer10PowerNbr(100);
                if(!allProperties)break;
            case simu_imageNbr :
                this.simRC.setImageNbr(1201);
                if(!allProperties)break;
            case simu_threadNbr :
                this.simRC.setThreadNbr(10);
        }
    }
}
