/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

/**
 *
 * @author Grégoire
 */
public interface MandelbrotDll extends Library{
        MandelbrotDll INSTANCE = (MandelbrotDll) Native.loadLibrary(
            "mand_cFunc",MandelbrotDll.class);
        public abstract int iterate(double z_r,double z_i,double c_r,double c_i,double maxIteration);
        public abstract void simpleMandelbrot(int width, int height,double resolution,double x0,double y0,int maxIteration,Pointer p);
    }