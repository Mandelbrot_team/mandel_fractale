package fractal.generator;





/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Grégoire
 */
public class ColorCalcGenerator {
    protected  String function;
     protected final  String BACK= ";\n";
     protected final  String FUNCTION_START="public static  int[] getRGB(int i,int max_iteration,double z_r){ \n";
     protected final  String RED ="int red = i"+BACK;
     protected final  String BLUE ="int blue = i"+BACK;
     protected final  String GREEN ="int green = i"+BACK;
     protected final  String RETURN ="return new int []{red%255,green%255,blue%255};\n" +" }";
      protected final  String DEFAULT_CALC= FUNCTION_START+RED+GREEN+BLUE+RETURN;
      
      
      
      protected final  String CLASS_START="public class ColorCalculator {";
      protected final  String CLASS_END="}";
      protected  String classString;
      
      protected final  String TEMP_DIR= System.getProperty("java.io.tmpdir");
      protected  final  String CLASS_NAME_JAVA="ColorCalculator.java";
      protected  final  String CLASS_NAME_CLASS="ColorCalculator.class";
      protected final String PATH_CLASS_FILE=OS.TEMP_FRACTALE_DIR+"\\"+CLASS_NAME_CLASS;
      protected final String PATH_JAVA_FILE=OS.TEMP_FRACTALE_DIR+"\\"+CLASS_NAME_JAVA;
      
      protected boolean compilationFailed=false;
      
     public ColorCalcGenerator(){
          function=DEFAULT_CALC;
          classString=getClassString();
      }
     public void writeFunction(){
        this.deleteFile(new File(PATH_JAVA_FILE));
        try {
            FileWriter writer = new FileWriter(new File(PATH_JAVA_FILE));
            writer.write(this.getClassString());
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
        
     }
     public void compileClass(){
        try {
            File file = new File(PATH_JAVA_FILE);
           Process p =  Runtime.getRuntime().exec("javac"+" "+PATH_JAVA_FILE);
            AfficheurFlux fluxSortie = new AfficheurFlux(p.getInputStream());
            AfficheurFlux fluxErreur = new AfficheurFlux(p.getErrorStream());
            new Thread(fluxSortie).start();
            new Thread(fluxErreur).start();
        } catch (IOException ex) {
            System.out.println(""+ex);
        }
     }
     public Method loadMethod(){
         Method res  =null;
        try {
            URL  url = OS.TEMP_FRACTALE_DIR.toURI().toURL();
            URLClassLoader loader = URLClassLoader.newInstance(new URL[]{url});
            Class theClass =loader.loadClass("ColorCalculator");
            Method[] met = theClass.getDeclaredMethods();
            for (int i = 0; i < met.length; i++) {
                if (met[i].getName().equals("getRGB")) {
                    res=met[i];
                    
                }
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(""+ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
     }
                    
          


     protected void deleteFile(File file){
        try {
            file.delete();
        } catch (Exception ex) {
            System.out.println(""+ex);
            System.out.println("Failed to del "+file.getName()+" at "+file.getPath());
        }
     }
     
     
     public void setClass(String text){
         this.function=text;
         
     }
     public   String getClassString(){
         return CLASS_START+"\n"+function+"\n"+CLASS_END;
         
     }
    protected  String getFunction() {
        return function;
    }
    

    public  void setFunction(String function) {
       this.function = function;
    }

    public String getBACK() {
        return BACK;
    }

    public String getFUNCTION_START() {
        return FUNCTION_START;
    }

    public String getRED() {
        return RED;
    }

    public String getBLUE() {
        return BLUE;
    }

    public String getGREEN() {
        return GREEN;
    }

    public String getRETURN() {
        return RETURN;
    }

    public String getDEFAULT_CALC() {
        return DEFAULT_CALC;
    }

    public String getCLASS_START() {
        return CLASS_START;
    }

    public String getCLASS_END() {
        return CLASS_END;
    }

    public String getTEMP_DIR() {
        return TEMP_DIR;
    }

    public String getCLASS_NAME_JAVA() {
        return CLASS_NAME_JAVA;
    }

    public String getCLASS_NAME_CLASS() {
        return CLASS_NAME_CLASS;
    }

    public String getPATH_CLASS_FILE() {
        return PATH_CLASS_FILE;
    }

    public String getPATH_JAVA_FILE() {
        return PATH_JAVA_FILE;
    }

    public boolean isCompilationFailed() {
        return compilationFailed;
    }
      
      public static void main(String[] args) {
          System.out.println("*****************************************************");
          ColorCalcGenerator c=new ColorCalcGenerator();
        System.out.println(c.getClassString());
        c.writeFunction();
          System.out.println(System.getProperty("java.io.tmpdir"));
          c.compileClass();
          Method met=c.loadMethod();
        try {
            System.out.println(((int [])met.invoke(null, new Object[]{10,30}))[1]);
        } catch (Exception ex) {
        }
        
    }
    class AfficheurFlux implements Runnable {

    private final InputStream inputStream;

    AfficheurFlux(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    private BufferedReader getBufferedReader(InputStream is) {
        return new BufferedReader(new InputStreamReader(is));
    }

    @Override
    public void run() {
        BufferedReader br = getBufferedReader(inputStream);
        String ligne = "";
        String res =null;
        try {
            while ((ligne = br.readLine()) != null) {
                System.out.println(ligne);
                res=res+ligne;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if (res==null) {
            compilationFailed=false;
            
        }
        if (res != null) {
            if (res.equals("")) {
                compilationFailed=false;
            }
        }
        else{
            System.out.println("FAILED "+res);
            compilationFailed=true;
        }
    }
}
}
