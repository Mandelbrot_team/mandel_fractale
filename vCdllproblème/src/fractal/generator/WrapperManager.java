/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;



/**
 *
 * @author Grégoire
 */
public class WrapperManager {
    
    private static Pointer arrayToPointer(double [][] array){
        Pointer p = new Memory(array.length*array[0].length*Native.getNativeSize(Double.TYPE));
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                p.setDouble((j*array.length+i)*Native.getNativeSize(Double.TYPE), array[i][j]);
            }
        }
        return p;
    }
    public static int[][] pointerToArray(int width,int height,Pointer p){
        int [][] res =new int[width][height];
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j <res[i].length ; j++) {
                res[i][j]=p.getInt((j*width+i)*Native.getNativeSize(Integer.TYPE));
            }
        }
        return res;
    }
    public static int[][] getI(int width, int height,double resolution,double x0,double y0,int maxIteration){
        Pointer p = new Memory(width*height*Native.getNativeSize(Integer.TYPE));
        for (int i = 0; i < width*height; i+=Native.getNativeSize(Integer.TYPE)) {
            p.setInt(i, 0);
        }
        MandelbrotDll.INSTANCE.simpleMandelbrot(width, height, resolution, x0, y0, maxIteration, p);
        
        return pointerToArray(width, height, p);
    }
    public static void main(String[] args) {
        Pointer p = new Memory(2*4*Native.getNativeSize(Integer.TYPE));
        int xy[][]=new int[2][4];
        for (int x = 0; x < xy.length; x++) {
            for (int y = 0; y < xy[x].length; y++) {
                xy[x][y]=x*xy[0].length+y;
                System.out.print(""+xy[x][y]);
                p.setInt(xy[x][y]*Native.getNativeSize(Integer.TYPE),xy[x][y] );
                
            }
            System.out.println("");
        }
        System.out.println("######");
        xy = pointerToArray(4, 2, p);
        MandelbrotDll.INSTANCE.iterate(2, 3, 1, 2, 12);
      
    }
}
