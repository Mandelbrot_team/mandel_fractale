package fractal.avec.thread;

// @author Raphaël

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

public class FractalGenerator extends JPanel implements ActionListener
{
    protected JPanel buttonBar;
    
        protected JButton screenshotButton;
        protected JButton explorationButton;
        //protected JButton vonKochInitializeButton;
        protected JButton simulationStartButton;
        
        protected JLabel stateLabel;
    protected GraphicPanel graphicPanel;
   protected Progress prog = new Progress();
    
    protected BufferedImage manImage; 
    protected String screenshotDirectory;
    protected GenerationConfig manGC, simGC;
    protected RenderConfig rendConf;
    protected double manualZoomMultiplicator;
    
    protected boolean goThreads = true ;
    
    public static final double MBx1 = -2.1, MBx2 = 0.6, MBy1 = -1.2, MBy2 = 1.2; // bornes de l'ensemble de Mandelbrot, sans zoom
    public static final int HDWidth = 1920, HDHeight = 1080;
    
    public FractalGenerator()
    {
        super(new BorderLayout());
       
                
        this.buttonBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
            this.screenshotButton = new JButton("Capture d'écran");
            this.explorationButton = new JButton("Exploration");
            //this.vonKochInitializeButton = new JButton("Initialiser Von Koch");
            this.simulationStartButton = new JButton("Simulation");
            
            this.stateLabel = new JLabel();
        this.graphicPanel = new GraphicPanel(this);
        
        this.add(this.buttonBar, BorderLayout.NORTH);
            this.buttonBar.add(this.screenshotButton);
            this.buttonBar.add(this.explorationButton);
            //this.buttonBar.add(this.vonKochInitializeButton);
            this.buttonBar.add(this.simulationStartButton);
            
//    
             
             
             this.add(this.stateLabel,BorderLayout.SOUTH);
        this.add(this.graphicPanel, BorderLayout.CENTER);
        
        this.screenshotButton.addActionListener(this);
        this.explorationButton.addActionListener(this);
        //this.vonKochInitializeButton.addActionListener(this);
        this.simulationStartButton.addActionListener(this);
        
        this.screenshotDirectory = "C:\\Users\\Raphaël\\Documents\\NetBeansProjects\\fractal avec thread\\screenshots\\";
        
        this.manGC = new GenerationConfig( // config de la generation en mode manuel
                new Point(-0.75, 0), // centre de la vue
                1, // zoom
                4, // carre du module limite
                this.graphicPanel.getWidth(), // largeur en pixel de l'image
                this.graphicPanel.getHeight(), // hauteur en pixel de l'image
                500); // nombre d'iterations maximal
        
        this.simGC = new GenerationConfig( // config de la generation en mode simulation
                new Point(-1.7732178424805036, -0.2885435049200177), // centre de la vue, a copier depuis la console
                1, // zoom
                4, // carre du module limite 
                HDWidth, // largeur en pixel de l'image
                HDHeight, // hauteur en pixel de l'image
                2000); // nombre d'iterations maximal
        
        this.rendConf = new RenderConfig( // config du rendu
                "D:\\", // dossier cible des frames
                "D:\\", // dossier cible de la video
                100, // nombre d'images par puissance de dix de zoom
                1201, // nombre d'images a generer
                10); // nombre de Threads simultannes
        
        this.manualZoomMultiplicator = 10;
    }
    
    public static void main(String[] args)
    {
        FractalGenerator mainPanel = new FractalGenerator();
        JFrame frame = new JFrame("Fractal Generator 2014");
        frame.setContentPane(mainPanel);
        
        //frame.addWindowListener((WindowListener)mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == this.screenshotButton)
        {
            File file;
            int i = 0;
                
            do
            {
                file = new File(screenshotDirectory + "screenshot_" + completeWithZeros(i, 6) + ".png");
                i++;
            } while(file.exists());
            
            saveImage(file, this.manImage);
        }
        else if(e.getSource() == this.explorationButton)
        {
            this.manualZoomMultiplicator = 10;
            this.manGC.getCenter().setCoordinates(-0.75, 0);
            this.manGC.setZoom(10);
            this.manGC.setWidth(this.graphicPanel.getWidth());
            this.manGC.setHeight(this.graphicPanel.getHeight());
            
            GenerationConfigFrameController gen = new GenerationConfigFrameController(manGC, this);
            gen.setVisible(true);
            System.out.println(this.information(this.manGC));
            this.repaint();
        }
        /*else if(e.getSource() == this.vonKochInitializeButton)
        {
            this.manImage = createVonKochFractal();
            this.repaint();
        }*/
        else if(e.getSource() == this.simulationStartButton)
        {
            RenderConfigFrameController ren = new RenderConfigFrameController(simGC, rendConf, this);
            ren.setVisible(true);
            
        }
         
        
        
    }
    
    public static BufferedImage createMandelbrotFractal(GenerationConfig gC)
    {
        BufferedImage img = new BufferedImage(gC.getWidth(), gC.getHeight(), BufferedImage.TYPE_INT_RGB);
        double DX = gC.getWidth(), // largeur image en pixel
               DY = gC.getHeight(), // hauteur image en pixel
               Dy = (MBy2-MBy1)/gC.getZoom(), // hauteur nouveau point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur nouveau point de vue en unite du plan
               resolution = DY/Dy, // nombre de pixels par unité du plan
               x0 = gC.getCenter().getAbs()-Dx/2, // nouvelles bornes avec zoom
               y0 = gC.getCenter().getOrd()-Dy/2,
               c_r, c_i, z_r, z_i, tmp; // valeurs reelles et imaginaires pour le calcul des termes de la suite
        int i;
        
        for(int x = 0; x < gC.getWidth(); x++)
        {
            for(int y = 0; y < gC.getHeight(); y++)
            {
                c_r = x / resolution + x0;
                c_i = y / resolution + y0;
                z_r = 0;
                z_i = 0;
                i = 0;

                do
                {
                    tmp = z_r;
                    z_r = z_r*z_r - z_i*z_i + c_r;
                    z_i = 2*z_i*tmp + c_i;
                    i++;
                } while(z_r*z_r + z_i*z_i < gC.getMaxModulus() && i < gC.getMaxIteration());

                if(i == gC.getMaxIteration())
                {
                    img.setRGB(x, y, 0);
                }
                else
                {
                    int red =(int)(((float)i/(gC.getMaxIteration()+0.0)*1000))%255;
                    int green = (int)(((float)i/(gC.getMaxIteration()+0.0)*5000))%255;
                    int blue=(int)(((float)i/(gC.getMaxIteration()+0.0)*200))%255;
                    img.setRGB(x, y, new Color(red, green, blue).getRGB());
                }
            }
        }
        
        return img;
    }
    public void startExploration(){
        this.manImage= createMandelbrotFractal(manGC);
         this.stateLabel.setText(this.information(this.manGC));
         System.out.println(this.information(this.manGC));
         this.repaint();
    }
    public void startSimulation(){
        ArrayList<Thread> threads = new ArrayList();
            
            java.awt.EventQueue.invokeLater(new Runnable()
            {
                public void run()
                {
                    prog.setVisible(true);
                }
            });
            
            this.simulationStartButton.setEnabled(false);
            
            for (int i = 0; i < this.rendConf.getThreadNbr(); i++)
            {
                threads.add(new Thread(new MandelRender(this.simGC, this.rendConf, i)));
                threads.get(i).start();
                //System.out.println(""+i);
            }
    }
    
    /*public void createVonKochFractal()
    {
        ArrayList<Point> points = new ArrayList();
        
        /*points.add(new Point(300, 200));
        points.add(new Point(900, 200));
        points.add(new Point(600, 600));
        
        points.add(new Point(300, 400));
        points.add(new Point(1000, 400));

        for(int i = 0; i < 8; i++)
        {
            applyIteration(points);
        }
        
        int[] xValues = new int[points.size()],
              yValues = new int[points.size()];
        
        for(int i = 0; i < points.size(); i++)
        {
            xValues[i] = (int)Math.round(points.get(i).getAbs());
            yValues[i] = (int)Math.round(points.get(i).getOrd());
        }

        this.image = new BufferedImage(this.graphicPanel.getWidth(), this.graphicPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = this.image.createGraphics();
        g.fillPolygon(xValues, yValues, points.size());
    }*/
    
    /*public static void applyIteration(ArrayList<Point> points)
    {
        double x1, y1, x2, y2, x3, y3;
        double newX1, newY1, xm, ym, newX2, newY2, newX3, newY3;
        double crossProductZ;
        
        for(int i = 0; i < points.size(); i += 4)
        {
            x1 = points.get(i).getAbs();
            y1 = points.get(i).getOrd();
            x2 = points.get((i+1)%points.size()).getAbs();
            y2 = points.get((i+1)%points.size()).getOrd();
            x3 = points.get((i+2)%points.size()).getAbs();
            y3 = points.get((i+2)%points.size()).getOrd();
            
            newX1 = 2*x1/3 + x2/3;
            newY1 = 2*y1/3 + y2/3;
            
            xm = (x1+x2)/2;
            ym = (y1+y2)/2;
            
            /*crossProductZ = (x2-x1)*(y3-y1)-(y2-y1)*(x3-x1); // composante sur Z du produit vectoriel
            
            if(crossProductZ < 0) // le point 3 est a droite du segment
            {
                newX2 = xm - Math.sqrt(3)*(y2-y1)/6;
                newY2 = ym + Math.sqrt(3)*(x2-x1)/6;
            /*}
            else
            {
                newX2 = xm + Math.sqrt(3)*(y2-y1)/6;
                newY2 = ym - Math.sqrt(3)*(x2-x1)/6;
            }
            
            newX3 = x1/3 + 2*x2/3;
            newY3 = y1/3 + 2*y2/3;
            
            points.add(i+1, new Point(newX1, newY1));
            points.add(i+2, new Point(newX2, newY2));
            points.add(i+3, new Point(newX3, newY3));
        }
    }*/
    
    public static void saveImage(File file, BufferedImage img)
    {
        if(img != null)
        {
            try
            {
                ImageIO.write(img, "PNG", file);
            }
            catch (IOException ex)
            {
                System.out.println("erreur " + img);
                Logger.getLogger(FractalGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void translate(GenerationConfig geneConf, int XC, int YC) // XC et YC sont les cordonnees sur l'image en pixels de la nouvelle position du centre
    {
        Point center = geneConf.getCenter();
        double DX = geneConf.getWidth(),
               DY = geneConf.getHeight(),
               Dy = (MBy2-MBy1)/geneConf.getZoom(), // hauteur point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur point de vue en unite du plan
               DxC = (XC/DX-0.5)*Dx, // deplacement centre de vue sur x en unite du plan
               DyC = (YC/DY-0.5)*Dy; // deplacement centre de vue sur y en unite du plan

        center.setCoordinates(center.getAbs()+DxC, center.getOrd()+DyC);
    }

    /**************************************************
     *          Threads Are Here                      *
     *                                                * 
     **************************************************/
    
    protected class MandelRender implements Runnable
    {
        private GenerationConfig geneConf;
        private RenderConfig rendConf;
        private int threadIndex;
            
        public MandelRender(GenerationConfig geneConf, RenderConfig rendConf, int threadIndex)
        {
            this.geneConf = geneConf;
            this.rendConf = rendConf;
            this.threadIndex = threadIndex;
        }

        @Override
        public void run()
        {
            int i = this.threadIndex;
            int j = 1;
            
            while (i < this.rendConf.getImageNbr() && prog.isGo())
            {
                this.geneConf.setZoom(Math.pow(10, (i/this.rendConf.getImagePer10PowerNbr())));
                BufferedImage img = createMandelbrotFractal(this.geneConf);
                saveImage(new File(this.rendConf.getVideoFrameDirectory()
                        + "frac_frame_"
                        + completeWithZeros(i, length(this.rendConf.getImageNbr()))
                        + ".png"),
                        img);
                System.out.println("Thread n°" + this.threadIndex + " : img n°" + i + " generated");
                i += this.rendConf.getThreadNbr();
                j++;
                prog.getjProgressBar1().setValue(prog.getjProgressBar1().getValue()+(int)((float)j/this.rendConf.getImageNbr()*100));
            }
        }
        
        /*
         * Mandelbrot method 
         */
        /*public BufferedImage createMandelbrotFractal(int XC, int YC, double zoomD, BufferedImage buffer )
    {
        double DX = graphicPanel.getWidth(), // largeur image en pixel
               DY = graphicPanel.getHeight(), // hauteur image en pixel
               Dy = (y2-y1)/zoom, // hauteur point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur point de vue en unite du plan
               DxC = (XC/DX-0.5)*Dx, // deplacement centre de vue sur x en unite du plan
               DyC = (YC/DY-0.5)*Dy; // deplacement centre de vue sur y en unite du plan
        /*System.out.println("XC = " + XC + " YC = " + YC
                + "\nDX = " + DX + " DY = " + DY
                + "\nDx = " + Dx + " Dy = " + Dy
                + "\nDxC = " + DxC + " DyC = " + DyC);
       centerAbs += DxC;
       centerOrd += DyC;
        
        Dy = (y2-y1)/zoom; // hauteur nouveau point de vue en unite du plan
        Dx = DX/DY*Dy; // largeur nouveau point de vue en unite du plan
        
        double resolution = DY/Dy; // nombre de pixels par unité du plan
        double iteration_max = 1000;
        double x0 = centerAbs-Dx/2, // nouvelles bornes avec zoom
               y0 =centerOrd-Dy/2;
        
       zoom = zoomD;
        //this.image = new BufferedImage((int)DX, (int)DY, BufferedImage.TYPE_INT_RGB);
        buffer= new BufferedImage((int)DX, (int)DY, BufferedImage.TYPE_INT_RGB);
       

        double c_r, c_i, z_r, z_i, tmp;
        int i;

        for(int x = 0; x < DX; x++)
        {
            for(int y = 0; y < DY; y++)
            {
                c_r = x / resolution + x0;
                c_i = y / resolution + y0;
                z_r = 0;
                z_i = 0;
                i = 0;

                do
                {
                    tmp = z_r;
                    z_r = z_r*z_r - z_i*z_i + c_r;
                    z_i = 2*z_i*tmp + c_i;
                    i = i+1;
                } while(z_r*z_r + z_i*z_i < 4 && i < iteration_max);

                if(i == iteration_max)
                {
                  //  this.image.setRGB(x, y, 0);
                    buffer.setRGB(x, y, 0);
                }
                else
                {
                    int red =(int)(((float)i/(iteration_max+0.0)*1000))%255;
                    int green = (int)(((float)i/(iteration_max+0.0)*5000))%255;
                    int blue=(int)(((float)i/(iteration_max+0.0)*200))%255;
                    //  this.image.setRGB(x, y, new Color(red, green, blue).getRGB());
                    buffer.setRGB(x, y, new Color(red, green, blue).getRGB());
                }
            }
        }
        
        return buffer;
    }*/
    }
    
    public static String completeWithZeros(int n, int totalSize)
    {
        String chain = Integer.toString(n);
        int chainLength = chain.length();
        
        for(int i = 0; i < totalSize-chainLength; i++)
        {
            chain = '0' + chain;
        }
        
        return chain;
    }
    
    public static int length(int n)
    {
        return Integer.toString(n).length();
    }
    
    public String information(GenerationConfig gC)
    {
        return "pos : (" + gC.getCenter().getAbs()
                + ", " + gC.getCenter().getOrd()
                + ") - zoom : " + gC.getZoom()
                + " - multiplicateur : " + (this.manualZoomMultiplicator < 1 ?
                1/(-this.manualZoomMultiplicator+2)
                : this.manualZoomMultiplicator);
    }
}
