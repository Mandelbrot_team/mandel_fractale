package fractal.generator;

// @author Raphaël

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class GraphicPanel extends JPanel implements MouseListener
{
    private FractalGenerator main;
    
    public GraphicPanel(FractalGenerator main)
    {
        this.main = main;
        
        this.addMouseListener(this);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {  
        if(this.main.image != null)
        {
            g.drawImage(this.main.image, this.getWidth()/2-this.main.image.getWidth()/2, this.getHeight()/2-this.main.image.getHeight()/2, null);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        
    }
    
    @Override
    public void mousePressed(MouseEvent e)
    {
        this.main.createMandelbrotFractal(e.getX(), e.getY(), this.main.zoom*10);
        this.repaint();
    }
    
    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
}
