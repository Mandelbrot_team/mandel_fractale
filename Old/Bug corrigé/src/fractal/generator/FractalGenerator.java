package fractal.generator;

// @author Raphaël

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FractalGenerator extends JPanel implements ActionListener
{
    private JPanel buttonBar;
        private JButton saveButton;
        private JButton mandelBrotButton;
        private JButton vonKochButton;
        private JButton startSimulation;
        private JLabel progressingLabel;
    private GraphicPanel graphicPanel;
    
    protected int FractalModel;
    protected BufferedImage image;
    protected double centerAbs, centerOrd;
    protected double zoom;
    
    public static double x1 = -2.1, x2 = 0.6, y1 = -1.2, y2 = 1.2; // bornes de l'ensemble de Mandelbrot, sans zoom
    
    public FractalGenerator()
    {
        super(new BorderLayout());
        
        this.buttonBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
            this.saveButton = new JButton("Sauvegarder");
            this.mandelBrotButton = new JButton("MandelBrot");
            this.vonKochButton = new JButton("Von Koch");
            this.startSimulation = new JButton("Commencer Simulation");
            this.progressingLabel = new JLabel();
        this.graphicPanel = new GraphicPanel(this);
        
        this.add(this.buttonBar, BorderLayout.NORTH);
            this.buttonBar.add(this.saveButton);
            this.buttonBar.add(this.mandelBrotButton);
            this.buttonBar.add(this.vonKochButton);
            this.buttonBar.add(this.startSimulation);
            this.buttonBar.add(this.progressingLabel);
        this.add(this.graphicPanel, BorderLayout.CENTER);
        
        this.saveButton.addActionListener(this);
        this.mandelBrotButton.addActionListener(this);
        this.vonKochButton.addActionListener(this);
        this.startSimulation.addActionListener(this);
        
        this.FractalModel = 0;
        this.zoom = 1;
    }
    
    public static void main(String[] args)
    {
        FractalGenerator mainPanel = new FractalGenerator();
        JFrame frame = new JFrame("Fractal Generator 2014");
        frame.setContentPane(mainPanel);
        //frame.addWindowListener((WindowListener)mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == this.saveButton)
        {
            File file;
            int i = 1;
                
            do
            {
                file = new File("C:\\Users\\raphael\\Documents\\NetBeansProjects\\Fractal Generator\\defaultDirectory\\fractal" + i);
                i++;
            } while(file.exists());
            
            this.saveImage(file);
        }
        else if(e.getSource() == this.mandelBrotButton)
        {
            this.FractalModel = 1;
            this.centerAbs = -0.75;
            this.centerOrd = 0;
            this.createMandelbrotFractal(this.graphicPanel.getWidth()/2, this.graphicPanel.getHeight()/2, 1);
            this.repaint();
        }
        else if(e.getSource() == this.vonKochButton)
        {
            this.FractalModel = 2;
            this.createVonKochFractal();
            this.repaint();
        }
        else if(e.getSource() == this.startSimulation)
        {
            this.saveButton.setEnabled(false);
            this.mandelBrotButton.setEnabled(false);
            this.vonKochButton.setEnabled(false);
            this.graphicPanel.setEnabled(false);
            
            this.FractalModel = 1;
            this.centerAbs = -0.7465499725019966;
            this.centerOrd = 0.10731688278947012;
            
            for(int i = 0; i <= 288; i++)
            {
                this.createMandelbrotFractal(this.graphicPanel.getWidth()/2, this.graphicPanel.getHeight()/2, Math.pow(10, i/24.0));
                this.saveImage(new File("C:\\Users\\raphael\\Documents\\NetBeansProjects\\Fractal Generator\\videoFrac\\imgFrac" + completeWithZeros(i, 3) + ".png"));
            }
            
            this.saveButton.setEnabled(true);
            this.mandelBrotButton.setEnabled(true);
            this.vonKochButton.setEnabled(true);
            this.graphicPanel.setEnabled(true);
        }
    }
    
    public void createMandelbrotFractal(int XC, int YC, double zoom)
    {
        double DX = this.graphicPanel.getWidth(), // largeur image en pixel
               DY = this.graphicPanel.getHeight(), // hauteur image en pixel
               Dy = (y2-y1)/this.zoom, // hauteur point de vue en unite du plan
               Dx = DX/DY*Dy, // largeur point de vue en unite du plan
               DxC = (XC/DX-0.5)*Dx, // deplacement centre de vue sur x en unite du plan
               DyC = (YC/DY-0.5)*Dy; // deplacement centre de vue sur y en unite du plan
        /*System.out.println("XC = " + XC + " YC = " + YC
                + "\nDX = " + DX + " DY = " + DY
                + "\nDx = " + Dx + " Dy = " + Dy
                + "\nDxC = " + DxC + " DyC = " + DyC);*/
        this.centerAbs += DxC;
        this.centerOrd += DyC;
        
        Dy = (y2-y1)/zoom; // hauteur nouveau point de vue en unite du plan
        Dx = DX/DY*Dy; // largeur nouveau point de vue en unite du plan
        
        double resolution = DY/Dy; // nombre de pixels par unité du plan
        double iteration_max = 1000;
        double x0 = this.centerAbs-Dx/2, // nouvelles bornes avec zoom
               y0 = this.centerOrd-Dy/2;
        
        this.zoom = zoom;
        this.image = new BufferedImage((int)DX, (int)DY, BufferedImage.TYPE_INT_RGB);

        double c_r, c_i, z_r, z_i, tmp;
        int i;

        for(int x = 0; x < DX; x++)
        {
            for(int y = 0; y < DY; y++)
            {
                c_r = x / resolution + x0;
                c_i = y / resolution + y0;
                z_r = 0;
                z_i = 0;
                i = 0;

                do
                {
                    tmp = z_r;
                    z_r = z_r*z_r - z_i*z_i + c_r;
                    z_i = 2*z_i*tmp + c_i;
                    i = i+1;
                } while(z_r*z_r + z_i*z_i < 4 && i < iteration_max);

                if(i == iteration_max)
                {
                    this.image.setRGB(x, y, 0);
                }
                else
                {
                    this.image.setRGB(x, y, i);
                }
            }
        }
    }
    
    public void createVonKochFractal()
    {
        ArrayList<Point> points = new ArrayList();
        
        /*points.add(new Point(300, 200));
        points.add(new Point(900, 200));
        points.add(new Point(600, 600));*/
        
        points.add(new Point(300, 400));
        points.add(new Point(1000, 400));

        for(int i = 0; i < 8; i++)
        {
            applyIteration(points);
        }
        
        int[] xValues = new int[points.size()],
              yValues = new int[points.size()];
        
        for(int i = 0; i < points.size(); i++)
        {
            xValues[i] = (int)Math.round(points.get(i).getAbs());
            yValues[i] = (int)Math.round(points.get(i).getOrd());
        }

        this.image = new BufferedImage(this.graphicPanel.getWidth(), this.graphicPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = this.image.createGraphics();
        g.fillPolygon(xValues, yValues, points.size());
    }
    
    public static void applyIteration(ArrayList<Point> points)
    {
        double x1, y1, x2, y2, x3, y3;
        double newX1, newY1, xm, ym, newX2, newY2, newX3, newY3;
        double crossProductZ;
        
        for(int i = 0; i < points.size(); i += 4)
        {
            x1 = points.get(i).getAbs();
            y1 = points.get(i).getOrd();
            x2 = points.get((i+1)%points.size()).getAbs();
            y2 = points.get((i+1)%points.size()).getOrd();
            x3 = points.get((i+2)%points.size()).getAbs();
            y3 = points.get((i+2)%points.size()).getOrd();
            
            newX1 = 2*x1/3 + x2/3;
            newY1 = 2*y1/3 + y2/3;
            
            xm = (x1+x2)/2;
            ym = (y1+y2)/2;
            
            /*crossProductZ = (x2-x1)*(y3-y1)-(y2-y1)*(x3-x1); // composante sur Z du produit vectoriel
            
            if(crossProductZ < 0) // le point 3 est a droite du segment
            {*/
                newX2 = xm - Math.sqrt(3)*(y2-y1)/6;
                newY2 = ym + Math.sqrt(3)*(x2-x1)/6;
            /*}
            else
            {
                newX2 = xm + Math.sqrt(3)*(y2-y1)/6;
                newY2 = ym - Math.sqrt(3)*(x2-x1)/6;
            }*/
            
            newX3 = x1/3 + 2*x2/3;
            newY3 = y1/3 + 2*y2/3;
            
            points.add(i+1, new Point(newX1, newY1));
            points.add(i+2, new Point(newX2, newY2));
            points.add(i+3, new Point(newX3, newY3));
        }
    }
    
    public void saveImage(File file)
    {
        if(this.image != null)
        {
            try
            {
                ImageIO.write(this.image, "PNG", file);
            }
            catch (IOException ex)
            {
                Logger.getLogger(FractalGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public String completeWithZeros(int n, int totalSize)
    {
        String chain = Integer.toString(n);
        int chainLength = chain.length();
        
        for(int i = 0; i < totalSize-chainLength; i++)
        {
            chain = '0' + chain;
        }
        
        return chain;
    }
}
