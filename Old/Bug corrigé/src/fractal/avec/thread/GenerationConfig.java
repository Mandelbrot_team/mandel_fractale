package fractal.avec.thread;

// @author Raphaël

public class GenerationConfig 
{
    private Point center;
    private double zoom, maxModulus;
    private int width, height, maxIteration;
    
    public GenerationConfig(Point center, double zoom, double maxModulus, int width, int height, int maxIteration)
    {
        this.center = center;
        this.zoom = zoom;
        this.maxModulus = maxModulus;
        this.width = width;
        this.height = height;
        this.maxIteration = maxIteration;
    }
    
    public Point getCenter()
    {
        return this.center;
    }
    
    public double getZoom()
    {
        return this.zoom;
    }
    
    public double getMaxModulus()
    {
        return this.maxModulus;
    }
    
    public int getWidth()
    {
        return this.width;
    }
    
    public int getHeight()
    {
        return this.height;
    }
    
    public int getMaxIteration()
    {
        return this.maxIteration;
    }
    
    public void setCenter(Point center)
    {
        this.center = center;
    }
    
    public void setZoom(double zoom)
    {
        this.zoom = zoom;
    }
    
    public void setMaxModulus(double maxModulus)
    {
        this.maxModulus = maxModulus;
    }
    
    public void setWidth(int width)
    {
        this.width = width;
    }
    
    public void setHeight(int height)
    {
        this.height = height;
    }
    
    public void setMaxIteration(int maxIteration)
    {
        this.maxIteration = maxIteration;
    }
    
    @Override
    public GenerationConfig clone() throws CloneNotSupportedException
    {
        return new GenerationConfig(
                this.center.clone(), this.zoom, this.maxModulus, this.width,
                this.height, this.maxIteration);
    }
}
