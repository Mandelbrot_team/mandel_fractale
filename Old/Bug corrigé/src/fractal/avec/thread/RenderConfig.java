package fractal.avec.thread;

// @author Raphaël

public class RenderConfig 
{
    private String videoFrameDirectory, videoDirectory;
    private double imagePer10PowerNbr;
    private int imageNbr, threadNbr;

    public RenderConfig(String videoFrameDirectory, String videoDirectory, double imagePer10PowerNbr, int imageNbr, int threadNbr)
    {
        this.videoFrameDirectory = videoFrameDirectory;
        this.videoDirectory = videoDirectory;
        this.imagePer10PowerNbr = imagePer10PowerNbr;
        this.imageNbr = imageNbr;
        this.threadNbr = threadNbr;
    }
    
    public String getVideoFrameDirectory()
    {
        return this.videoFrameDirectory;
    }
    
    public String getVideoDirectory()
    {
        return this.videoDirectory;
    }
    
    public double getImagePer10PowerNbr()
    {
        return this.imagePer10PowerNbr;
    }
    
    public int getImageNbr()
    {
        return this.imageNbr;
    }
    
    public int getThreadNbr()
    {
        return this.threadNbr;
    }
    
    public void setVideoFrameDirectory(String videoFrameDirectory)
    {
        this.videoFrameDirectory = videoFrameDirectory;
    }
    
    public void setVideoDirectory(String videoDirectory)
    {
        this.videoDirectory = videoDirectory;
    }
    
    public void setImagePer10PowerNbr(double imagePer10PowerNbr)
    {
        this.imagePer10PowerNbr = imagePer10PowerNbr;
    }
    
    public void setImageNbr(int imageNbr)
    {
        this.imageNbr = imageNbr;
    }
    
    public void setThreadNbr(int threadNbr)
    {
        this.threadNbr = threadNbr;
    }
}
