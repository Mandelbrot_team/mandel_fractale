package fractal.avec.thread;

// @author Raphaël

public class Point
{
    private double abs, ord;
    
    public Point(double abs, double ord)
    {
        this.abs = abs;
        this.ord = ord;
    }
    
    public double getAbs()
    {
        return this.abs;
    }
    
    public double getOrd()
    {
        return this.ord;
    }
    
    public void setAbs(double abs)
    {
        this.abs = abs;
    }
    
    public void setOrd(double ord)
    {
        this.ord = ord;
    }
    
    public void setCoordinates(double abs, double ord)
    {
        this.abs = abs;
        this.ord = ord;
    }
    
    @Override
    public Point clone() throws CloneNotSupportedException
    {
        return new Point(this.abs, this.ord);
    }
}
