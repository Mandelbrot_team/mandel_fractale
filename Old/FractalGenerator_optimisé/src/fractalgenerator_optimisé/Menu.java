/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractalgenerator_optimisé;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Grégoire
 */
public  class Menu extends JMenuBar implements ActionListener{
        JMenu file = new JMenu("file");
        JMenuItem save = new JMenuItem("save ");
        JMenuItem open = new JMenuItem("open");
        GenerationConfigFrameController parent;
        File fileInput;
        public Menu( GenerationConfigFrameController parent){
            this.parent=parent;
            this.fileInput=fileInput;
            this.file.add(this.save);
            this.file.add(this.open);
            this.add(this.file);
            
            this.save.addActionListener(this);
            this.open.addActionListener(this);
                    
        }

        @Override
        public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.save){
            JFileChooser chooser = new JFileChooser();
                int returnVal = chooser.showOpenDialog(this);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                   System.out.println("You chose to open this file: " +
                        chooser.getSelectedFile().getName());
                     this.parent.setCurrentConfFile(chooser.getSelectedFile());
                        
                    }
                
            
        }
        else if (e.getSource() == this.open){
            JFileChooser chooser = new JFileChooser();
                int returnVal = chooser.showOpenDialog(this);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                   System.out.println("You chose to open this file: " +
                        chooser.getSelectedFile().getName());
                        String path =chooser.getSelectedFile().getAbsolutePath();
                        String[] tempS =path.split(".");
                        try {
                        if(!tempS[tempS.length-1].equals(".vect")){
                            path+=".vect";
                        }
                        
                    } catch (Exception e5) {
                        path+=".vect";
                    }
                        
                        
                    }
            
        }
        
        }
    }