package fractalgenerator_optimisé;

// @author Raphaël

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;


public class GenerationConfig 
{
    private Point center;
    private double zoom, maxModulus;
    private int width, height, maxIteration;
    
    public GenerationConfig(Point center, double zoom, double maxModulus, int width, int height, int maxIteration)
    {
        this.center = center;
        this.zoom = zoom;
        this.maxModulus = maxModulus;
        this.width = width;
        this.height = height;
        this.maxIteration = maxIteration;
    }
    
    public Point getCenter()
    {
        return this.center;
    }
    
    public double getZoom()
    {
        return this.zoom;
    }
    
    public double getMaxModulus()
    {
        return this.maxModulus;
    }
    
    public int getWidth()
    {
        return this.width;
    }
    
    public int getHeight()
    {
        return this.height;
    }
    
    public int getMaxIteration()
    {
        return this.maxIteration;
    }
    
    public void setCenter(Point center)
    {
        this.center = center;
    }
    
    public void setZoom(double zoom)
    {
        this.zoom = zoom;
    }
    
    public void setMaxModulus(double maxModulus)
    {
        this.maxModulus = maxModulus;
    }
    
    public void setWidth(int width)
    {
        this.width = width;
    }
    
    public void setHeight(int height)
    {
        this.height = height;
    }
    
    public void setMaxIteration(int maxIteration)
    {
        this.maxIteration = maxIteration;
    }
    /*
     * point x,y
     * 
     */
    public String saveConf(){
        String res ="";
        res+=this.center.getAbs()+" "+this.center.getOrd()+"\n"; // first Line
        res+=this.zoom+"\n";
        res+=this.maxIteration+"\n";
        res+=this.width+"\n";
        res+=this.height+"\n";
        return res;
                
    }
    public static GenerationConfig readConf(File file) throws FileNotFoundException, IOException{
        Point center = null;
        BufferedReader buff = new BufferedReader(new FileReader(file));
        String [] tempStrings=buff.readLine().split(" ");
        center= new  Point(Double.parseDouble(tempStrings[0]), Double.parseDouble(tempStrings[1]));
        
        tempStrings=buff.readLine().split(" ");
        int zoom=Integer.parseInt(tempStrings[0]);
        
        tempStrings=buff.readLine().split(" ");
        int maxIteration = Integer.parseInt(tempStrings[0]);
        
        tempStrings=buff.readLine().split(" ");
         int width = Integer.parseInt(tempStrings[0]);
         int height  = Integer.parseInt(tempStrings[1]);
       
        return new GenerationConfig(center, zoom, 2, width, height, maxIteration);
        
    }
}
