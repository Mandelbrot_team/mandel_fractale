/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfSelector;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.nio.Buffer;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.SoftBevelBorder;

/**
 *
 * @author Grégoire
 */
public class ListRenderer  implements ListCellRenderer{
    public final static Dimension PANEL_DIMENSION= new Dimension(200, 200);
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel render = (JLabel)defaultRenderer.getListCellRendererComponent(list, "", index, isSelected, cellHasFocus);
        
        render.setPreferredSize(PANEL_DIMENSION);
        render.setIcon((Icon)value);
        render.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) { 
                System.out.println("lololo");
            
            } @Override
            public void mousePressed(MouseEvent e) { }@Override
            public void mouseReleased(MouseEvent e) {}  @Override
            public void mouseEntered(MouseEvent e) { } @Override
            public void mouseExited(MouseEvent e) { }
        });
        if (cellHasFocus) {
            render.setBorder(BorderFactory.createBevelBorder(SoftBevelBorder.RAISED, Color.lightGray, Color.yellow));
        }
        return render;
    
    }
    class CellList extends JPanel{
        private BufferedImage img ;
        public CellList(BufferedImage img){
            this.img=img;
            this.repaint();
        }
        public void paint(Graphics g){
            g.drawImage( img,0,0,null);
        }
    }
    
}
