/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal.generator;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grégoire
 */
public class OS {
    public static final  File TEMP_DIR = new File(System.getProperty("java.io.tmpdir"));
    public static final File TEMP_FRACTALE_DIR = new File(TEMP_DIR+"\\FRACTALE_GENERATOR_2014");
    public static final File DIR_APP_DATA= new File(System.getenv("APPDATA"));
    public static final File DIR_APP_DATA_FRACTALE=new File(DIR_APP_DATA+"\\FractalGenerator2014");
    
    public static final String INIT_FILE_NAME="fractale.ini";
    
    public static final File INIT_FILE= new File(DIR_APP_DATA_FRACTALE+"\\"+INIT_FILE_NAME);
    public static void  createAppDataFolder(){
        DIR_APP_DATA_FRACTALE.mkdir();
    }
    public static void checkEssentialFolder(){
        System.out.println(""+TEMP_FRACTALE_DIR.getAbsolutePath());
        if(!TEMP_FRACTALE_DIR.exists()){
           
            
                TEMP_FRACTALE_DIR.mkdir();
           
                
        }
    }
    public static void clearTempFile(){ // clears from the upper folder the temp .java and .class filesn not the configs ones
        String fileRem []= getFilesEndingWith(TEMP_FRACTALE_DIR.list() ,new String[]{".java",".class"});
        if (fileRem != null) {
           for (int i = 0; i <fileRem.length; i++) {
            clearFileFromPath(new File(TEMP_FRACTALE_DIR+"\\"+fileRem[i]));
        } 
        }
        
    }
    public static void clearFileFromPath(File file){
        file.delete();
    }
    public static String [] getFilesEndingWith(String []files,String [] types){
        
       ArrayList<String> arr = new ArrayList();
       for (int i = 0; i < files.length; i++) {
           if (isEndingWith(files[i], types)) {
              arr.add(files[i]);
           }
       }
       String res []= new String [arr.size()];
       for (int i = 0; i < res.length; i++) {
           res[i]=arr.get(i);
       }
       return res;
   }
    public static boolean isEndingWith(String  in,String [] ext){
      boolean   res=false;
        for (int i = 0; i < ext.length; i++) {
            if (in.endsWith(ext[i])) {
                  res =res || true;
            }
          
        }
        return res;
        
    }
    public static String buildInitContent(String [] in ){
        String res = "";
        for (int i = 0; i < in.length; i++) {
            res =res +in[i]+"\n";
        }
        return res ;
        
    }
    public static String [] readInitFile(){
        String inString=readFromTxtFile(INIT_FILE);
       
       return  inString.split(System.getProperty("line.separator"));
        
    }
    public static void  writeInitFile(String inString[]){
        String res ="";
        for (int i = 0; i < inString.length; i++) {
            res=res+inString[i]+ System.getProperty("line.separator");
        }
        writeTextFile(DIR_APP_DATA_FRACTALE, INIT_FILE_NAME , res);
    }
    
    public static void writeTextFile(File folder,String name,String in){
        try {
            System.out.println("to write : \n"+in);
            File fil = new File(folder.getPath()+"\\"+name);
            System.out.println(DIR_APP_DATA_FRACTALE
                    );
            fil.createNewFile();
            FileWriter f = new FileWriter(fil);
            BufferedWriter buff = new BufferedWriter(f);
            buff.write(in);
            buff.close();
            f.close();
        } catch (IOException ex) {
            Logger.getLogger(OS.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    }
    public static boolean checkInitFile(){
        if(INIT_FILE.exists()){
            System.out.println("ini"+INIT_FILE.getAbsolutePath());
            String res []= readInitFile();
            boolean arg1 = new File(res[0]).isDirectory();
            boolean arg2=new File(res[1]).isDirectory();
            return arg1  ;
            
        }
        else{
        return false;
    }
        
    }
    public  static String readFromTxtFile(File file ){
        String res = "";
        FileReader f = null;
        try {
            f = new FileReader(file);
            BufferedReader buff = new BufferedReader(f);
             String temp="";
            while( temp!= null){
                temp=buff.readLine();
                if (temp != null) {
                      res=res+temp+ System.getProperty("line.separator");
                }
          
            
            
            
            }
            f.close();
        
            
        } catch (Exception ex) {
        }
        return res;

    }
    public static void main(String[] args) {
        System.out.println(""+readInitFile());
    }
    
}
