package fractal.generator;



// @author Raphaël

import java.util.ArrayList;


public class GenerationMatrix
{
    private boolean[][] matrix;
    
    public GenerationMatrix(int width, int height)
    {
        this.matrix = new boolean[width][height];
    }
    
    public GenerationMatrix(boolean[][] matrix)
    {
        this.matrix = matrix;
    }
    
    public boolean[][] getMatrix()
    {
        return this.matrix;
    }
    
    public void setMatrix(boolean[][] matrix)
    {
        this.matrix = matrix;
    }
    
    public GenerationMatrix getPortion(int x, int y, int width, int height)
    {
        boolean[][] portion = new boolean[width][height];
        
        for(int i = 0; i < width; i++)
        {
            for(int j = 0; j < height; j++)
            {
                if(x+i < 0 || x+i >= this.matrix.length || y+j < 0 || y+j >= this.matrix[0].length)
                {
                    portion[i][j] = false;
                }
                else
                {
                    portion[i][j] = this.matrix[x+i][y+j];
                }
            }
        }
        
        return new GenerationMatrix(portion);
    }
    
    public void stretch(int newWidth, int newHeight)
    {
        boolean[][] horStretched = new boolean[newWidth][this.matrix[0].length],
                    stretched = new boolean[newWidth][newHeight];
        double sizeSum;
        int sequelStart, sequelSize, sequelEnd;
        boolean boolValue;
        
        // etirement horizontal
        for(int j = 0; j < this.matrix[0].length; j++)
        {
            sizeSum = 0;
            int i = 0;
            
            while(i < this.matrix.length)
            {
                sequelStart = i;
                sequelSize = 0;
                boolValue = this.matrix[i][j];
                
                do
                {
                    sequelSize++;
                    i++;
                    
                    if(i == this.matrix.length)
                    {
                        break;
                    }
                }while(this.matrix[i][j] == boolValue);
                
                sequelStart = sequelStart*newWidth/this.matrix.length;
                sizeSum += sequelSize*newWidth/(this.matrix.length+0.0);
                sequelEnd = (int)sizeSum;
                
                for(int k = sequelStart; k < sequelEnd; k++)
                {
                    horStretched[k][j] = boolValue;
                }
            }
        }
        
        //etirement vertical
        for(int i = 0; i < newWidth; i++)
        {
            sizeSum = 0;
            int j = 0;
            
            while(j < this.matrix[0].length)
            {
                sequelStart = j;
                sequelSize = 0;
                boolValue = horStretched[i][j];
                
                do
                {
                    sequelSize++;
                    j++;
                    
                    if(j == this.matrix[0].length)
                    {
                        break;
                    }
                }while(horStretched[i][j] == boolValue);
                
                sequelStart = sequelStart*newHeight/this.matrix[0].length;
                sizeSum += sequelSize*newHeight/(this.matrix[0].length+0.0);
                sequelEnd = (int)sizeSum;
                
                for(int k = sequelStart; k < sequelEnd; k++)
                {
                    stretched[i][k] = boolValue;
                }
            }
        }
        
        this.matrix = stretched;
    }
    
    public void trim() // rogner les groupes de 1
    {
        ArrayList<Integer> borderPointsX = new ArrayList(),
                           borderPointsY = new ArrayList();
        boolean leftExists, rightExists, upExists, downExists, pointAdded;
        
        for(int i = 0; i < this.matrix.length; i++)
        {
            for(int j = 0; j < this.matrix[0].length; j++)
            {
                pointAdded = false;
                
                while(this.matrix[i][j])
                {
                    leftExists = i > 0;
                    rightExists = i+1 < this.matrix.length;
                    upExists = j > 0;
                    downExists = j+1 < this.matrix[0].length;

                    if(rightExists)
                    {
                        if(!this.matrix[i+1][j])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(rightExists && downExists)
                    {
                        if(!this.matrix[i+1][j+1])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(downExists)
                    {
                        if(!this.matrix[i][j+1])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(downExists && leftExists)
                    {
                        if(!this.matrix[i-1][j+1])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(leftExists)
                    {
                        if(!this.matrix[i-1][j])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(leftExists && upExists)
                    {
                        if(!this.matrix[i-1][j-1])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(upExists)
                    {
                        if(!this.matrix[i][j-1])
                        {
                            pointAdded = true;
                            break;
                        }
                    }

                    if(upExists && rightExists)
                    {
                        if(!this.matrix[i+1][j-1])
                        {
                            pointAdded = true;
                            break;
                        }
                    }
                    
                    break;
                }
                
                if(pointAdded)
                {
                    borderPointsX.add(new Integer(i));
                    borderPointsY.add(new Integer(j));
                }
            }
        }
        
        for(int i = 0; i < borderPointsX.size(); i++)
        {
            this.matrix[borderPointsX.get(i).intValue()][borderPointsY.get(i).intValue()] = false;
        }
    }
    
    @Override
    public String toString()
    {
        String result = "";
        
        for(int j = 0; j < this.matrix[0].length; j++)
        {
            for(int i = 0; i < this.matrix.length; i++)
            {
                result += this.matrix[i][j] ? " 1" : " 0";
            }
            
            result += '\n';
        }
        
        return result;
    }
}
