/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Grégoire
 */
public class ColorConfig {
    private BufferedImage exmapleImage;
    private String function ;
    private String name;
    private File currentLocation ;
    private final static String DEFAULT_COLOR_CONFIG_NAME = "default.color";
    public final static File DEFAULT_COLOR_CONFIG_PATH= new File(OS.DIR_APP_DATA_FRACTALE,DEFAULT_COLOR_CONFIG_NAME); 
    public final static ColorConfig DEFAULT_COLOR_CONFIG;
    static {
        DEFAULT_COLOR_CONFIG=getDefaultColorConfig();
    }
    public ColorConfig(BufferedImage exmapleImage, String function, String name,File currentLocation) {
        this.exmapleImage = exmapleImage;
        this.function = function;
        this.name = name;
        this.currentLocation=currentLocation;
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

            
    
    
    public BufferedImage getExmapleImage() {
        return exmapleImage;
    }

    public void setExmapleImage(BufferedImage exmapleImage) {
        this.exmapleImage = exmapleImage;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String funtion) {
        this.function = funtion;
    }

    public File getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(File currentLocation) {
        this.currentLocation = currentLocation;
    }
    
   private static ColorConfig getDefaultColorConfig(){
       String function = ColorCalcGenerator.DEFAULT_CALC;
       BufferedImage defBuffer = new BufferedImage(500, 300, BufferedImage.TYPE_INT_RGB);
       Graphics2D g =(Graphics2D )defBuffer.createGraphics();
       g.setFont(new Font("calibri", Font.BOLD, 70));
       g.drawString("DEFAULT IMAGE", 0,150);
       ColorConfig res=  new ColorConfig(defBuffer, function, "default",new File(OS.DIR_APP_DATA_FRACTALE,"default.color"));
       return res;
       
   }
   public static void main(String args[]){
      getDefaultColorConfig();
   }
}
