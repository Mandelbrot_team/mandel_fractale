package fractal.generator;



// @author Raphaël

import static fractal.generator.FractalGenerator.MBy1;
import static fractal.generator.FractalGenerator.MBy2;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class GraphicPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener
{
    private FractalGenerator parent;
    private Point dynamicPos;
    private BufferedImage finalImage;
    private BufferedImage pinImage;
    private NoBackgroundImage pinNBImage;
    
    public GraphicPanel(FractalGenerator parent)
    {
        this.parent = parent;
        
        try
        {   
            
            this.pinImage = ImageIO.read(getClass().getResource("./images/punaise.png"));
            this.pinNBImage = new NoBackgroundImage(this.pinImage);
        }
        catch (IOException ex)
        {
            Logger.getLogger(GraphicPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {  
        super.paintComponent(g);
        
        if(this.parent.exploImage != null)
        {
            g.drawImage(this.parent.exploImage,
                    this.getWidth()/2-this.parent.exploImage.getWidth()/2,
                    this.getHeight()/2-this.parent.exploImage.getHeight()/2, null);
            
            double Dy = (MBy2-MBy1)/this.parent.exploGC.getZoom(), // hauteur nouveau point de vue en unite du plan
                   resolution = this.parent.exploGC.getHeight()/Dy; // nombre de pixels par unité du plan
            int pinX, pinY;
            
            for(int i = 0; i < this.parent.pins.size(); i++)
            {
                if(this.parent.exploGC.getZoom() <= this.parent.pins.get(i).getZoom())
                {
                    pinX = (int)((this.parent.pins.get(i).getPosition().getAbs()-this.parent.exploGC.getCenter().getAbs()) * resolution + this.getWidth()/2 - this.pinImage.getWidth()/2);
                    pinY = (int)((this.parent.pins.get(i).getPosition().getOrd()-this.parent.exploGC.getCenter().getOrd()) * resolution + this.getHeight()/2 - this.pinImage.getHeight());
                    this.pinNBImage.printOnGraphics(g, pinX, pinY);
                }
            }
            
            if(this.dynamicPos != null)
            {
                if(this.parent.holdingAPin)
                {
                    this.pinNBImage.printOnGraphics(g, (int)(this.dynamicPos.getAbs()-this.pinImage.getWidth()/2),
                            (int)(this.dynamicPos.getOrd()-this.pinImage.getHeight()));
                }
                else
                {
                    double DX = this.parent.exploGC.getWidth()/this.parent.exploZoomMultiplicator,
                           DY = this.parent.exploGC.getHeight()/this.parent.exploZoomMultiplicator;

                    g.setColor(Color.WHITE);
                    g.drawRect((int)(this.dynamicPos.getAbs()-DX/2),
                            (int)(this.dynamicPos.getOrd()-DY/2), (int)DX, (int)DY);
                }
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        
    }
    
    @Override
    public void mousePressed(MouseEvent e)
    {
        if(this.parent.exploImage != null)
        {
            if(e.getButton() == MouseEvent.BUTTON1)
            {
                if(this.parent.holdingAPin) // on place une épingle
                {
                    double DY = this.parent.exploGC.getHeight(), // hauteur image en pixel
                           Dy = (MBy2-MBy1)/this.parent.exploGC.getZoom(), // hauteur nouveau point de vue en unite du plan
                           resolution = DY/Dy, // nombre de pixels par unité du plan
                           x = this.parent.exploGC.getCenter().getAbs() + (e.getX()-this.getWidth()/2) / resolution,
                           y = this.parent.exploGC.getCenter().getOrd() + (e.getY()-this.getHeight()/2) / resolution;
                    Point pressPosition = new Point(x, y);
                    
                    this.parent.pins.add(new Pin("Point de vue " + (this.parent.pins.size()+1), pressPosition, this.parent.exploGC.getZoom()));
                    this.parent.holdingAPin = false;
                    this.repaint();
                }
                else // on zoom
                {
                    int newX = e.getX()-this.getWidth()/2+this.parent.exploGC.getWidth()/2, // position cliquee sur l'image
                        newY = e.getY()-this.getHeight()/2+this.parent.exploGC.getHeight()/2,
                        portionWidth = (int)(this.parent.exploGC.getWidth()/this.parent.exploZoomMultiplicator), // dimensions du cadre de zoom
                        portionHeight = (int)(this.parent.exploGC.getHeight()/this.parent.exploZoomMultiplicator);

                    FractalGenerator.translate(this.parent.exploGC, newX, newY);
                    this.parent.exploGMatrix = this.parent.exploGMatrix.getPortion(newX-portionWidth/2, newY-portionHeight/2, portionWidth, portionHeight);

                    for(int i = 0; i < 7; i++)
                    {
                        this.parent.exploGMatrix.trim();
                    }

                    this.parent.exploGMatrix.stretch(this.parent.exploGC.getWidth(), this.parent.exploGC.getHeight());
                    this.parent.exploGC.setZoom(this.parent.exploGC.getZoom()*this.parent.exploZoomMultiplicator);
                    this.parent.exploImage = this.parent.createMandelbrotFractal(this.parent.exploGC, this.parent.exploGMatrix, 10);
                    
                    this.repaint();
                }
            }
            else if(e.getButton() == MouseEvent.BUTTON3)
            {
                int newX = e.getX()-this.getWidth()/2+this.parent.exploGC.getWidth()/2, // position cliquee sur l'image
                    newY = e.getY()-this.getHeight()/2+this.parent.exploGC.getHeight()/2;
                
                FractalGenerator.translate(this.parent.exploGC, newX, newY);
                this.parent.exploGC.setZoom(this.parent.exploGC.getZoom()/this.parent.exploZoomMultiplicator);
                this.parent.exploGMatrix = new GenerationMatrix(this.parent.exploGC.getWidth(), this.parent.exploGC.getHeight());
                this.parent.exploImage = this.parent.createMandelbrotFractal(this.parent.exploGC, this.parent.exploGMatrix, 10);
                this.repaint();
            }

            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            System.out.println(this.parent.information(this.parent.exploGC));
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseDragged(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e)
    {
        this.dynamicPos = new Point(e.getX(), e.getY());
        this.repaint();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        if(e.getWheelRotation() > 0 && this.parent.exploZoomMultiplicator > 1.0)
        {
            this.parent.exploZoomMultiplicator--;
            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            this.repaint();
        }
        else if(e.getWheelRotation() < 0 && this.parent.exploZoomMultiplicator < 100.0)
        {
            this.parent.exploZoomMultiplicator++;
            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            this.repaint();
        }
    }
    
    public static void printImageOnImage(BufferedImage imageToPrint, BufferedImage support, int x, int y)
    {
        if(x+imageToPrint.getWidth() > 0 && x < support.getWidth()
                && y+imageToPrint.getHeight() > 0 && y < support.getHeight())
        {
            for(int i = 0; i < imageToPrint.getWidth(); i++)
            {
                for(int j = 0; j < imageToPrint.getHeight(); j++)
                {
                    if(x+i >= 0 && x+i < support.getWidth() && y+j >= 0 && y+j < support.getHeight())
                    {
                        support.setRGB(x+i, y+j, imageToPrint.getRGB(i, j));
                    }
                }
            }
        }
    }
}
