package fractal.generator;





/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Grégoire
 */
public class ColorCalcGenerator {
    protected  String function;
   
     protected static final  String BACK= ";\n";
     protected static final  String FUNCTION_START="public static  int[] getRGB(int i,int max_iteration,double z_r){ \n";
     protected static final  String RED ="double red = i"+BACK;
     protected static final  String BLUE ="double blue = i"+BACK;
     protected static final  String GREEN ="double green = i"+BACK;
     protected static final  String RETURN ="return new int []{(int)red%255,(int)green%255,(int)blue%255};\n" +" }";
        protected static final String EXPO ="public static double exp(double d){"+"\n"
                                  +"return Math.exp(d); \n }";
       protected static final String LN ="public static double ln(double d){"+"\n"
                                  +"return Math.log(d); \n }";
       protected static final String COS ="public static double cos(double d){"+"\n"
                                  +"return Math.cos(d); \n }";
       protected static final String SIN ="public static double sin(double d){"+"\n"
                                  +"return Math.sin(d); \n }";
       protected static final String TAN ="public static double tan(double d){"+"\n"
                                  +"return Math.tan(d); \n }";
       protected static final String ABS ="public static double abs(double d){"+"\n"
                                  +"return Math.abs(d); \n }";
      protected static final String RGB_FUNC =FUNCTION_START+RED+GREEN+BLUE+RETURN;
      protected static final  String DEFAULT_CALC= RGB_FUNC+"\n"+EXPO+"\n"+LN+"\n"+SIN+"\n"+COS+
                            "\n"+TAN+"\n"+ABS;
      
      
      
      protected static final  String CLASS_START="public class ColorCalculator {";
      protected static final  String CLASS_END="}";
      protected String classString;
      
      protected static final  String TEMP_DIR= System.getProperty("java.io.tmpdir");
      protected static final  String CLASS_NAME_JAVA="ColorCalculator.java";
      protected static final  String CLASS_NAME_CLASS="ColorCalculator.class";
      protected static final String PATH_CLASS_FILE=OS.TEMP_FRACTALE_DIR+"\\"+CLASS_NAME_CLASS;
      protected static final String PATH_JAVA_FILE=OS.TEMP_FRACTALE_DIR+"\\"+CLASS_NAME_JAVA;
      
      protected boolean compilationFailed=false;
       protected static final File CURRENT_FUNCTION_FILE =new File(PATH_JAVA_FILE, "currentFunction");
     public ColorCalcGenerator(){
          function=DEFAULT_CALC;
          classString=getClassString();
        
      }
     @Deprecated
     public void writeFunctionDeprecated(){
        this.deleteFile(new File(PATH_JAVA_FILE));
        try {
           
            FileWriter writer = new FileWriter(new File(PATH_JAVA_FILE));
            System.out.println(PATH_JAVA_FILE);
            writer.write(this.getClassString());
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
        
     }
      public static void writeFunction(String function){
        deleteFile(new File(PATH_JAVA_FILE));
        try {
           
            FileWriter writer = new FileWriter(new File(PATH_JAVA_FILE));
            System.out.println(PATH_JAVA_FILE);
            String classJava = CLASS_START +"\n"+function+"\n"+CLASS_END;
            writer.write(classJava);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         
        
     }
     
     @Deprecated
     public void compileClassDepre(){
        try {
            System.out.println(""+PATH_JAVA_FILE);
            File file = new File(PATH_JAVA_FILE);
            System.out.println("from compile "+file.exists());
           Process p =  Runtime.getRuntime().exec("javac"+" "+PATH_JAVA_FILE);
            AfficheurFlux fluxSortie = new AfficheurFlux(p.getInputStream());
            AfficheurFlux fluxErreur = new AfficheurFlux(p.getErrorStream());
            new Thread(fluxSortie).start();
            new Thread(fluxErreur).start();
        } catch (IOException ex) {
            System.out.println(""+ex);
        }
     }
     public static void compileClass(){
        try {
            System.out.println(""+PATH_JAVA_FILE);
            File file = new File(PATH_JAVA_FILE);
            System.out.println("from compile "+file.exists());
           Process p =  Runtime.getRuntime().exec("javac"+" "+PATH_JAVA_FILE);
            
        } catch (IOException ex) {
            System.out.println(""+ex);
        }
     }
     @Deprecated
     public Method loadMethodDeprecated(){
         Method res  =null;
        try {
            URL  url = OS.TEMP_FRACTALE_DIR.toURI().toURL();
            System.out.println(" DIR APT DATA "+OS.DIR_APP_DATA_FRACTALE.getAbsolutePath());
            URLClassLoader loader = URLClassLoader.newInstance(new URL[]{url});
            Class theClass =loader.loadClass("ColorCalculator");
            Method[] met = theClass.getDeclaredMethods();
            for (int i = 0; i < met.length; i++) {
                System.out.println(""+met[i].getName());
                if (met[i].getName().equals("getRGB")) {
                    res=met[i];
                    
                }
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(""+ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
     }
         public static Method loadMethod(){
         Method res  =null;
        try {
            URL  url = OS.TEMP_FRACTALE_DIR.toURI().toURL();
            System.out.println(" DIR APT DATA "+OS.DIR_APP_DATA_FRACTALE.getAbsolutePath());
            URLClassLoader loader = URLClassLoader.newInstance(new URL[]{url});
            Class theClass =loader.loadClass("ColorCalculator");
            Method[] met = theClass.getDeclaredMethods();
            for (int i = 0; i < met.length; i++) {
                System.out.println(""+met[i].getName());
                if (met[i].getName().equals("getRGB")) {
                    res=met[i];
                    
                }
            }
            
        } catch (ClassNotFoundException ex) {
            System.out.println(""+ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
     }           
          


     public static  void deleteFile(File file){
        try {
            file.delete();
        } catch (Exception ex) {
            System.out.println(""+ex);
            System.out.println("Failed to del "+file.getName()+" at "+file.getPath());
        }
     }
     
     
     public void setClass(String text){
         this.function=text;
         
     }
     public   String getClassString(){
         return CLASS_START+"\n"+function+"\n"+CLASS_END;
         
     }
    protected  String getFunction() {
        return function;
    }
    

    public  void setFunction(String function) {
       this.function = function;
    }


    public boolean isCompilationFailed() {
        return compilationFailed;
    }
      
      public static void main(String[] args) {
         
       
        
    }
      @Deprecated
    class AfficheurFlux implements Runnable {

    private final InputStream inputStream;

    AfficheurFlux(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    private BufferedReader getBufferedReader(InputStream is) {
        return new BufferedReader(new InputStreamReader(is));
    }

    @Override
    public void run() {
        BufferedReader br = getBufferedReader(inputStream);
        String ligne = "";
        String res =null;
        try {
            while ((ligne = br.readLine()) != null) {
                System.out.println(ligne);
                res=res+ligne;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if (res==null) {
            compilationFailed=false;
            
        }
        if (res != null) {
            if (res.equals("")) {
                compilationFailed=false;
            }
        }
        else{
            System.out.println("FAILED "+res);
            compilationFailed=true;
        }
    }
    }
    public static String getCurrentFunction(){
        String res ="";
        if (CURRENT_FUNCTION_FILE.exists()) {
            
        try {
            FileReader reader = new FileReader(CURRENT_FUNCTION_FILE);
            BufferedReader buRead = new BufferedReader(reader);
            String line ="";
            while((line=buRead.readLine())!=null){
                res+=line+"\n";
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }   catch (IOException ex) {
                Logger.getLogger(ColorCalcGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
            return res;
        }
        else{
            return DEFAULT_CALC;
        }
    }

}
