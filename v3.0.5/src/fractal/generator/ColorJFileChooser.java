/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileView;

/**
 *
 * @author Grégoire
 */
public class ColorJFileChooser extends JFileChooser {
    public ColorJFileChooser(){
        super();
        this.setFileView(new ColorConfigView());
        this.setAccessory(new ImagePreview(this));
        this.setCurrentDirectory(new File("C:\\Users\\Grégoire\\Desktop\\"));
    }
    class ColorConfigView extends FileView{

        @Override
        public Boolean isTraversable(File f) {
           
            return super.isTraversable(f); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Icon getIcon(File f) {
            Icon res=null;
            if (f.getName().endsWith("color")) {
                ColorConfig tempC = ColorIO.read(f);
                BufferedImage tempBuffer = tempC.getExmapleImage();
                res = new ImageIcon(ColorIO.resizeImage(tempBuffer, 15, 15));
            }
            return res;
        }

        @Override
        public String getTypeDescription(File f) {
          return f.getName().endsWith("color")?"Color Model":"";
        
        }

        @Override
        public String getDescription(File f) {
            return super.getDescription(f); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getName(File f) {
            return super.getName(f); //To change body of generated methods, choose Tools | Templates.
        }
        
    }
    class ImagePreview extends JComponent
                          implements PropertyChangeListener {
    ImageIcon thumbnail = null;
    File file = null;

    public ImagePreview(JFileChooser fc) {
        setPreferredSize(new Dimension(200, 30));
        fc.addPropertyChangeListener(this);
    }

    public void loadImage() {
        if (file == null) {
            thumbnail = null;
            return;
        }
        if(!file.getName().endsWith("color")){
            return ;
        }

        //Don't use createImageIcon (which is a wrapper for getResource)
        //because the image we're trying to load is probably not one
        //of this program's own resources.
        ColorConfig colTemp = ColorIO.read(file);
        BufferedImage buff = colTemp.getExmapleImage();
        
        thumbnail= new ImageIcon(ColorIO.resizeImage(buff, 200,100));
        
    }
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        boolean update = false;
        String prop = e.getPropertyName();

        //If the directory changed, don't show an image.
        if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(prop)) {
            file = null;
            update = true;

        //If a file became selected, find out which one.
        } else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(prop)) {
            file = (File) e.getNewValue();
            update = true;
        }

        //Update the preview accordingly.
        if (update) {
            thumbnail = null;
            if (isShowing()) {
                loadImage();
                repaint();
            }
        }
    }

    protected void paintComponent(Graphics g) {
        if (thumbnail == null) {
            loadImage();
           
        }
        if (thumbnail != null) {
            thumbnail.paintIcon(this, g, 0, 0);
            
        }
    }
}
    public static void main(String[] args) {
        ColorJFileChooser ch = new ColorJFileChooser();
        int re = ch.showOpenDialog(null);
    }
    
}
