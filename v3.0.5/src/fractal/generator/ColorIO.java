/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fractal.generator;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

/**
 *
 * @author Grégoire
 */
public class ColorIO {
    public static final File TEMP_COLOR_FILE =new File(OS.DIR_APP_DATA_FRACTALE.getAbsolutePath()+"\\"+"tempColor.color");
    public static final File TEMP_IMG_FILE = new File(OS.DIR_APP_DATA_FRACTALE.getAbsolutePath()+"\\"+"tempImg.png");
    
    private static void writeColor(String function){
        
        try {
            FileWriter writer = new FileWriter(TEMP_COLOR_FILE);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(function);
            bufferedWriter.close();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
   private static void writeImage(BufferedImage buffer){
        try {
            BufferedImage bufferScaled=resizeImage(buffer, 300, (int)(300*buffer.getHeight()/buffer.getWidth()));
            ImageIO.write(bufferScaled, "png", TEMP_IMG_FILE);
        } catch (IOException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
   public  static BufferedImage resizeImage(BufferedImage image, int width, int height) {
         int type=0;
        type = image.getType() == 0? BufferedImage.TYPE_INT_ARGB : image.getType();
        BufferedImage resizedImage = new BufferedImage(width, height,type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
     }
    private static void addToZip(ZipOutputStream zipStream , File file){
        try {
            FileInputStream fileStream = new FileInputStream(file);
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zipStream.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fileStream.read(bytes)) >= 0) {
			zipStream.write(bytes, 0, length);
		}
            zipStream.closeEntry();
            fileStream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        }
       
   }
  private static void createColorFile(String function , BufferedImage buff,File colorFile){
       writeColor(function);
       writeImage(buff);
        try {
            FileOutputStream fileStream = new FileOutputStream(colorFile);
            ZipOutputStream zipStream = new ZipOutputStream(fileStream);
            addToZip(zipStream, TEMP_COLOR_FILE);
            addToZip(zipStream, TEMP_IMG_FILE);
            zipStream.close();
            fileStream.close();
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
    public static void write(ColorConfig config){
        createColorFile(config.getFunction(), config.getExmapleImage(),config.getCurrentLocation());
    }
   private static  String readTxtFileFromZip(ZipInputStream zipStream){
       byte[] buffer = new byte[2048];
       String res="";
       int len =0;
        try {
            while ((len = zipStream.read(buffer)) > 0)
            {
               res+= new String(buffer);
            }
        } catch (IOException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
   }
   private  static BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
    public static ColorConfig  read (File colFile){
        ColorConfig conf = null;
        try {
            InputStream iStream = null ;
            ColorConfig res=  null;
            BufferedImage buffered ;
            String function;
            String name=""  ;
            String fileName = colFile.getName();
            String [] fileNameSplitted = fileName.split(".");
            for (int i = 0; i < fileNameSplitted.length-1; i++) {
                name+=fileNameSplitted[i];
            }   iStream = new FileInputStream(colFile);
            
            ZipFile zip = new ZipFile(colFile);
            ZipEntry functionEntry = zip.getEntry(TEMP_COLOR_FILE.getName());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = zip.getInputStream(functionEntry);
            byte[] buffer = new byte[2048];
            for (int n; (n = in.read(buffer)) != -1; )  
                out.write(buffer, 0, n); 
            function=new String(out.toByteArray());
            out.close();
            in.close();
           
            out = new ByteArrayOutputStream();
            ZipEntry imageEntry  = zip.getEntry(TEMP_IMG_FILE.getName());
            in= zip.getInputStream(imageEntry);
            buffer = new byte[2048];
            for (int n; (n = in.read(buffer)) != -1; )  
                out.write(buffer, 0, n); 
            in.close();
            in = new ByteArrayInputStream(out.toByteArray());
            buffered =ImageIO.read(in);
            out.close();
            conf = new ColorConfig(buffered, function, name,colFile);
            return conf;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
           
        } catch (IOException ex) {
            Logger.getLogger(ColorIO.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(""+conf.getFunction());
       return conf;
    }
    public static void main(String[] args) throws IOException {
    }
    
}
