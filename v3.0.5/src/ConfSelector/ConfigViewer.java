/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfSelector;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Grégoire
 */
public class ConfigViewer  extends JPanel implements ListSelectionListener {
    public final static Dimension PANEL_DIMENSION= new Dimension(200, 200);
     JList confList;
    public ConfigViewer(){
        confList= new JList(getIcon(new File("D:\\")));
        this.setLayout(new BorderLayout());
        confList.setCellRenderer(new ListRenderer());
        JScrollPane jScroll = new JScrollPane(confList);
        jScroll.setPreferredSize(new Dimension (ListRenderer.PANEL_DIMENSION.width,this.getHeight()));
        this.add(jScroll,BorderLayout.CENTER);
        confList.addListSelectionListener(this);
    }
    public static Icon[] getIcon(File folder){
        Icon res [] =null;
        if (folder.isDirectory()) {
           String [] files = folder.list();
          String pngFiles[]=getJPGFiles(files);
          res= new Icon[pngFiles.length];
            for (int i = 0; i < pngFiles.length; i++) {
                res[i]=new ImageIcon(folder.getPath()+"\\"+pngFiles[i]);
            }
         
        }
        return res;
    }
    public static BufferedImage[] getBufferdImg(File folder){
        BufferedImage res [] =null;
        if (folder.isDirectory()) {
           String [] files = folder.list();
          String pngFiles[]=getJPGFiles(files);
          res=getPngArray(pngFiles,folder);
        }
        return res;
    }
   public static String [] getJPGFiles(String []files){
       ArrayList<String> arr = new ArrayList();
       for (int i = 0; i < files.length; i++) {
           if (files[i].endsWith(".png") || files[i].endsWith(".jpg")) {
              arr.add(files[i]);
           }
       }
       String res []= new String [arr.size()];
       for (int i = 0; i < res.length; i++) {
           res[i]=arr.get(i);
       }
       return res;
   }
   public static BufferedImage[] getPngArray(String [] pngFiles,File folder){
       BufferedImage [] res= new BufferedImage[pngFiles.length];
       
       try {
           for (int i = 0; i < pngFiles.length; i++) {
              res[i]= normalise(ImageIO.read(new File(folder.getPath()+"\\"+pngFiles[i])));
           }
       } catch (Exception e) {
       }
       return res;
   }
   public static  BufferedImage normalise(BufferedImage img ){
        
        double height =  ListRenderer.PANEL_DIMENSION.getHeight();
        double rapport = img.getWidth()/height;
        
        return toBufferedImage(img.getScaledInstance((int)(img.getWidth()/rapport),(int) height , Image.SCALE_DEFAULT));
       
   }
 
   
    public static void main(String[] args) {
        BufferedImage [] res = getBufferdImg(new File("D:\\"));
        for (int i = 0; i < res.length; i++) {
        }
        JFrame j = new JFrame();
        j.setSize(new Dimension(400,1200));
        j.add(new ConfigViewer());
        j.setVisible(true);
    }
    public static BufferedImage toBufferedImage(Image img)
{
    if (img instanceof BufferedImage)
    {
        return (BufferedImage) img;
    }

    // Create a buffered image with transparency
    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

    // Draw the image on to the buffered image
    Graphics2D bGr = bimage.createGraphics();
    bGr.drawImage(img, 0, 0, null);
    bGr.dispose();

    // Return the buffered image
    return bimage;
}

    @Override
    public void valueChanged(ListSelectionEvent e) {
            if(e.getSource() == confList ){
                e.getFirstIndex();
            }
    
    }
    
}
