package fractal.generator;

// @author Raphaël

abstract class GraphicalObject
{
    protected Sprite sprite;
    protected IntPoint screenPos;
    
    public Sprite getSprite()
    {
        return this.sprite;
    }
    
    public IntPoint getScreenPos()
    {
        return this.screenPos;
    }
    
    public void setSprite(Sprite sprite)
    {
        this.sprite = sprite;
    }
    
    public void setScreenPos(IntPoint screenPos)
    {
        this.screenPos = screenPos;
    }
    
    public boolean checkCollision(int x, int y)
    {
        int XPosOnSprite = x - this.screenPos.getAbs() + this.sprite.getOrigin().getAbs(),
            YPosOnSprite = y - this.screenPos.getOrd() + this.sprite.getOrigin().getOrd();
        
        if(XPosOnSprite > -1 && XPosOnSprite < this.sprite.getWidth()
                && YPosOnSprite > -1 && YPosOnSprite < this.sprite.getHeight())
        {
            if(!this.sprite.isBackground(XPosOnSprite, YPosOnSprite))
            {
                return true;
            }
        }
        
        return false;
    }
}
