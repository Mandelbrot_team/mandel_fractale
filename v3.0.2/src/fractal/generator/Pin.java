package fractal.generator;

// @author Raphaël

public class Pin extends GraphicalObject
{
    private String name;
    private Point realPos;
    private double zoom;
    
    public Pin(String name, Sprite sprite, Point realPos, double zoom)
    {
        this.name = name;
        this.sprite = sprite;
        this.realPos = realPos;
        this.zoom = zoom;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public Point getRealPos()
    {
        return this.realPos;
    }
    
    public double getZoom()
    {
        return this.zoom;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setRealPos(Point realPos)
    {
        this.realPos = realPos;
    }
    
    public void setZoom(double zoom)
    {
        this.zoom = zoom;
    }
}
