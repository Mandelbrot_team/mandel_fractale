package fractal.generator;

// @author Raphaël

public class IntPoint
{
    private int abs, ord;
    
    public IntPoint()
    {
        
    }
    
    public IntPoint(int abs, int ord)
    {
        this.abs = abs;
        this.ord = ord;
    }
    
    public int getAbs()
    {
        return this.abs;
    }
    
    public int getOrd()
    {
        return this.ord;
    }
    
    public void setAbs(int abs)
    {
        this.abs = abs;
    }
    
    public void setOrd(int ord)
    {
        this.ord = ord;
    }
    
    public void setCoordinates(int abs, int ord)
    {
        this.abs = abs;
        this.ord = ord;
    }
    
    @Override
    public IntPoint clone() throws CloneNotSupportedException
    {
        return new IntPoint(this.abs, this.ord);
    }
}
