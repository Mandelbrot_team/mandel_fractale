package fractal.generator;

// fonctions de Mise En Page
public class Layout
{
    //--------------------------------------------------------------------------
    public static String spaces(int length) // permet d'obtenir un espacement d'une certaine length
    {
        String result;
        int i;

        result = "";
        
        for(i = 0; i < length; i++)
        {
            result += " ";
        }
        
        return result;
    } // fin de String spaces(int length)

    
    //--------------------------------------------------------------------------
    public static String line(int length, char c) // permet d'obtenir une ligne faite d'un motif donne
    {
        String result;
        int i;
        
        result = "";
        
        for(i = 0; i < length; i++)
        {
            result += c;
        }
        
        return result;
    } // fin de String ligne(int length, char c)
    

    //--------------------------------------------------------------------------
    public static int length(int number) // calcule le nombre de caractères d'un entier
    {
        String s;

        s = Integer.toString(number);
        return s.length();
    } // fin de int length(int nombre)
    
    
    //--------------------------------------------------------------------------
    public static String forcePlace(String s, int place, boolean complete) // permet de limiter un mot a une place donnee
    { // complete dit s'il faut complete avec des spaces ou non dans le cas ou la place donnee serait trop grande
        if(place <= 0)
        {
            return "";
        }
        
        if(s.length() > place)
        {
            if(place == 1)
            {
                return ".";
            }
            
            if(place == 2)
            {
                return "..";
            }
            
            return s.substring(0, place-3) + "...";
        }
        
        return s + (complete ? spaces(place-s.length()) : "");
    } // fin de forcePlace(String s, int place)
    
    
    //--------------------------------------------------------------------------
    public static String forcePlaceDouble(double x, int place, boolean complete) // permet de forcer la place d'un reel
    {
        String converted, mantissa, exponent;
        int i;
        
        converted = Double.toString(x);
        mantissa = "";
        exponent = "";
        i = 0;
        
        while(i < converted.length())
        {
            if(converted.charAt(i) == 'E')
            {
                exponent = converted.substring(i);
                break;
            }
            
            mantissa += converted.charAt(i);
            i++;
        }
        
        return forcePlace(forcePlace(mantissa, place-exponent.length(), false) + exponent, place, complete);
    }
    
    
    //--------------------------------------------------------------------------
    public static String plural(String mot)
    {
        return mot + "s"; // cas general
    }
    
    
    //--------------------------------------------------------------------------
    public static String capital(String expression) // met une majuscule a la premiere lettre trouvee
    {
        String result;
        int i;
        
        result = "";
        i = 0;
        
        while(i < expression.length())
        {
            if(isALetter(expression.charAt(i)))
            {
                result += toUpperCase(expression.charAt(i));
                i++;
                break;
            }
            
            result += expression.charAt(i);
            i++;
            
            if(i == expression.length())
            {
                break;
            }
        }
        
        while(i < expression.length())
        {
            result += expression.charAt(i);
            i++;
            
            if(i == expression.length())
            {
                break;
            }
        }
        
        return result;
    }
    
    
    //--------------------------------------------------------------------------
    public static String transDet(String expression)
    {
        String remplaced, substitute;
        int pos, posMin;
        
        posMin = expression.indexOf("le ");
        remplaced = "le ";
        substitute = "un ";
        
        pos = expression.indexOf("l'");
        if(pos != -1 && (pos < posMin || posMin == -1))
        {
            posMin = pos;
            remplaced = "l'";
            substitute = "un ";
        }
        
        pos = expression.indexOf("la ");
        if(pos != -1 && (pos < posMin || posMin == -1))
        {
            posMin = pos;
            remplaced = "la ";
            substitute = "une ";
        }
        
        pos = expression.indexOf("les ");
        if(pos != -1 && (pos < posMin || posMin == -1))
        {
            posMin = pos;
            remplaced = "les ";
            substitute = "des ";
        }
        
        pos = expression.indexOf("un ");
        if(pos != -1 && (pos < posMin || posMin == -1))
        {
            posMin = pos;
            remplaced = "un ";
            substitute = "le ";
        }
        
        pos = expression.indexOf("une ");
        if(pos != -1 && (pos < posMin || posMin == -1))
        {
            posMin = pos;
            remplaced = "une ";
            substitute = "la ";
        }
        
        pos = expression.indexOf("des ");
        if(pos != -1 && (pos < posMin || posMin == -1))
        {
            remplaced = "des ";
            substitute = "les ";
        }
        
        return expression.replaceFirst(remplaced, substitute);
    }
    
    
    //--------------------------------------------------------------------------
    public static boolean isALetter(char c)
    {
        return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';
    }
    
    
    //--------------------------------------------------------------------------
    public static char toUpperCase(char c)
    {
        String s;
        
        s = "" + c;
        
        if(isALetter(c))
        {    
            return s.toUpperCase().charAt(0);
        }
        else
        {
            return c;
        }
    }
}
