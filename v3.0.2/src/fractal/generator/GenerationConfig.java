package fractal.generator;



// @author Raphaël

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GenerationConfig 
{
    private Point center;
    private double zoom;
    private int width, height, maxIteration;
    protected ColorCalcGenerator generator=new ColorCalcGenerator();
    protected Method met = generator.loadMethod();
    public GenerationConfig()
    {
        this.center = new Point();
    }
    
    public GenerationConfig(Point center, double zoom, int width, int height, int maxIteration)
    {
        this.center = center;
        this.zoom = zoom;
        this.width = width;
        this.height = height;
        this.maxIteration = maxIteration;
    }
    public void updateMethod(){
        this.met=generator.loadMethod();
    }
    public ColorCalcGenerator getGenerator() {
        return generator;
    }

    public void setGenerator(ColorCalcGenerator generator) {
        this.generator = generator;
    }

    public Method getMet() {
        return met;
    }

    public void setMet(Method met) {
        this.met = met;
    }
    public void loadMethod(){
        this.met= this.generator.loadMethod();
    }
    public int[] getRGB(int i,double z_r) {
       int [] res =null;
            try {
            res=((int [])this.met.invoke(null, new Object[]{i,this.maxIteration,z_r}));
        } catch (Exception e) {
                System.err.println("ERROR ");
        }
            return res;
       
    }
    public Point getCenter()
    {
        return this.center;
    }
    
    public double getZoom()
    {
        return this.zoom;
    }
    
    public int getWidth()
    {
        return this.width;
    }
    
    public int getHeight()
    {
        return this.height;
    }
    
    public int getMaxIteration()
    {
        return this.maxIteration;
    }
    
    public void setCenter(Point center)
    {
        this.center = center;
    }
    
    public void setZoom(double zoom)
    {
        this.zoom = zoom;
    }
    
    public void setWidth(int width)
    {
        this.width = width;
    }
    
    public void setHeight(int height)
    {
        this.height = height;
    }
    
    public void setMaxIteration(int maxIteration)
    {
        this.maxIteration = maxIteration;
    }
    
    @Override
    public GenerationConfig clone() throws CloneNotSupportedException
    {
        return new GenerationConfig(this.center.clone(), this.zoom, this.width,
                this.height, this.maxIteration);
    }
    
    /*
     * point x,y
     * 
     */
    public String saveConf(){
        String res ="";
        res+=this.center.getAbs()+" "+this.center.getOrd()+"\n"; // first Line
        res+=this.zoom+"\n";
        res+=this.maxIteration+"\n";
        res+=this.width+"\n";
        res+=this.height+"\n";
        return res;
                
    }
    public static GenerationConfig readConf(File file) throws FileNotFoundException, IOException{
        Point center = null;
        BufferedReader buff = new BufferedReader(new FileReader(file));
        String [] tempStrings=buff.readLine().split(" ");
        center= new  Point(Double.parseDouble(tempStrings[0]), Double.parseDouble(tempStrings[1]));
        
        tempStrings=buff.readLine().split(" ");
        int zoom=Integer.parseInt(tempStrings[0]);
        
        tempStrings=buff.readLine().split(" ");
        int maxIteration = Integer.parseInt(tempStrings[0]);
        
        tempStrings=buff.readLine().split(" ");
         int width = Integer.parseInt(tempStrings[0]);
         int height  = Integer.parseInt(tempStrings[1]);
       
        return new GenerationConfig(center, zoom, width, height, maxIteration);
        
    }
}
