

// @author Raphaël

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.JPanel;


public class GraphicPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener
{
    private FractalGenerator parent;
    private Point dynamicPos;
    
    public GraphicPanel(FractalGenerator parent)
    {
        this.parent = parent;
        
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {  
        super.paintComponent(g);
        
        if(this.parent.exploImage != null)
        {
            g.drawImage(
                    this.parent.exploImage,
                    this.getWidth()/2-this.parent.exploImage.getWidth()/2,
                    this.getHeight()/2-this.parent.exploImage.getHeight()/2,
                    null);
            
            if(this.dynamicPos != null)
            {
                double DX = this.parent.exploGC.getWidth()/this.parent.exploZoomMultiplicator,
                       DY = this.parent.exploGC.getHeight()/this.parent.exploZoomMultiplicator;

                g.drawRect((int)(this.dynamicPos.getAbs()-DX/2),
                        (int)(this.dynamicPos.getOrd()-DY/2), (int)DX, (int)DY);
            }
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        
    }
    
    @Override
    public void mousePressed(MouseEvent e)
    {
        if(this.parent.exploImage != null)
        {
            if(e.getButton() == MouseEvent.BUTTON1)
            {
                int newX = e.getX()-this.getWidth()/2+this.parent.exploGC.getWidth()/2, // position cliquee sur l'image
                    newY = e.getY()-this.getHeight()/2+this.parent.exploGC.getHeight()/2,
                    portionWidth = (int)(this.parent.exploGC.getWidth()/this.parent.exploZoomMultiplicator), // dimensions du cadre de zoom
                    portionHeight = (int)(this.parent.exploGC.getHeight()/this.parent.exploZoomMultiplicator);
                
                FractalGenerator.translate(this.parent.exploGC, newX, newY);
                this.parent.exploGMatrix = this.parent.exploGMatrix.getPortion(newX-portionWidth/2, newY-portionHeight/2, portionWidth, portionHeight);
                
                for(int i = 0; i < 7; i++)
                {
                    this.parent.exploGMatrix.trim();
                }

                this.parent.exploGMatrix.stretch(this.parent.exploGC.getWidth(), this.parent.exploGC.getHeight());
                this.parent.exploGC.setZoom(this.parent.exploGC.getZoom()*this.parent.exploZoomMultiplicator);
                this.parent.exploImage = this.parent.createMandelbrotFractal(this.parent.exploGC, this.parent.exploGMatrix, 10);
                this.repaint();
            }
            else if(e.getButton() == MouseEvent.BUTTON3)
            {
                int newX = e.getX()-this.getWidth()/2+this.parent.exploGC.getWidth()/2, // position cliquee sur l'image
                    newY = e.getY()-this.getHeight()/2+this.parent.exploGC.getHeight()/2;
                
                FractalGenerator.translate(this.parent.exploGC, newX, newY);
                this.parent.exploGC.setZoom(this.parent.exploGC.getZoom()/this.parent.exploZoomMultiplicator);
                this.parent.exploGMatrix = new GenerationMatrix(this.parent.exploGC.getWidth(), this.parent.exploGC.getHeight());
                this.parent.exploImage = this.parent.createMandelbrotFractal(this.parent.exploGC, this.parent.exploGMatrix, 10);
                this.repaint();
            }

            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            System.out.println(this.parent.information(this.parent.exploGC));
        }
    }
    
    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseDragged(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e)
    {
        this.dynamicPos = new Point(e.getX(), e.getY());
        this.repaint();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        if(e.getWheelRotation() > 0 && this.parent.exploZoomMultiplicator > 1.0)
        {
            this.parent.exploZoomMultiplicator--;
            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            this.repaint();
        }
        else if(e.getWheelRotation() < 0 && this.parent.exploZoomMultiplicator < 100.0)
        {
            this.parent.exploZoomMultiplicator++;
            this.parent.stateLabel.setText(this.parent.information(this.parent.exploGC));
            this.repaint();
        }
    }
}
