/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal.avec.thread;
/**
 *
 * @author Grégoire
 */
public class MegaDouble{
   protected  int powerNumber=5; // nombre de puissances stokées + ou - 
    protected int sign; // ou -1
    protected final int BASE=Integer.MAX_VALUE/2; // normal Integer.MAX_VALUE/2
    protected int [] megaDoublePowerPos = new int[this.powerNumber];
     protected int [] megaDoublePowerNeg = new int[this.powerNumber];
   protected int [] megaDouble = new int [this.powerNumber*2+1];
    
    public MegaDouble(int [] posPower,int[] negPower,int sign){
        this.sign=sign; // 1 ou -1
        if(posPower.length>this.powerNumber ||negPower.length>this.powerNumber ){
            System.out.println("ERRROR fatal");
        }
        else{
            for (int i = 0; i < posPower.length; i++) {
                megaDouble[this.powerNumber-i]=posPower[i];
                
                
            }
            for (int i = 0; i < negPower.length; i++) {
                megaDouble[powerNumber+1+i]=negPower[i];
                
            }
        }
    }
   public MegaDouble(int [] megaDouble ,int sign ){
       this.megaDouble=megaDouble;
       this.sign=sign;
   }
    public static String  p(int[] tab){
        String res = "";
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i]+" ");
            res+=tab[i]+" ";
        }
        System.out.println("");
        return res+" ";
    }
    public MegaDouble add (MegaDouble megaDouble2){
        MegaDouble megaDouble1= megaDouble2.clone();
        
        for (int i = this.megaDouble.length-1; i >=0 ; i--) {
            if(i-1>=0){
                
                int posInt =megaDouble[i]+megaDouble1.getMegaDouble()[i];
                this.megaDouble[i]=posInt%BASE;
                this.megaDouble[i-1]+=posInt/BASE;
             
            }
            
        }
        
        
       return this;
        
    }

    public int getPowerNumber() {
        return powerNumber;
    }

    public void setPowerNumber(int powerNumber) {
        this.powerNumber = powerNumber;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public int[] getMegaDoublePowerPos() {
        return megaDoublePowerPos;
    }

    public void setMegaDoublePowerPos(int[] megaDoublePowerPos) {
        this.megaDoublePowerPos = megaDoublePowerPos;
    }

    public int[] getMegaDoublePowerNeg() {
        return megaDoublePowerNeg;
    }

    

    public int[] getMegaDouble() {
        return megaDouble;
    }

    public void setMegaDouble(int[] megaDouble) {
        this.megaDouble = megaDouble;
    }
    public MegaDouble clone(){
        
       return new MegaDouble(copyTab(megaDouble), sign);
        
    }
    public static int[] copyTab( int [] tab1){
        int [] res = new int[tab1.length];
        for (int i = 0; i < res.length; i++) {
            res[i]=tab1[i];
            
        }
        return res;
    }
    public String toString(){
        return p(megaDoublePowerPos)+ " --- \n"+p(megaDoublePowerNeg); 
    }
            
    public static void main(String[] args) {
        
        MegaDouble m = new  MegaDouble(new int[]{1,1}, new int[]{Integer.MAX_VALUE/2},1);
         p(m.getMegaDouble());
         m.add(m);
         p(m.getMegaDouble());
        
    }
      public static final MegaDouble MEGA_ZERO= new MegaDouble(new int[]{0}, new int[]{0},1);
    
}
