package fractal.avec.thread;

// @author Raphaël

import fractal.avec.thread.FractalGenerator;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;


public class GraphicPanel extends JPanel implements MouseListener, MouseMotionListener, MouseWheelListener
{
    private FractalGenerator main;
    private Point dynamicPos;
    
    public GraphicPanel(FractalGenerator main)
    {
        this.main = main;
        
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {  
        super.paintComponent(g);
        
        if(this.main.manImage != null)
        {
            g.drawImage(
                    this.main.manImage,
                    this.getWidth()/2-this.main.manImage.getWidth()/2,
                    this.getHeight()/2-this.main.manImage.getHeight()/2,
                    null);
        }
        
        if(this.dynamicPos != null)
        {
            double DX = this.main.manGC.getWidth()/this.main.manualZoomMultiplicator,
                   DY = this.main.manGC.getHeight()/this.main.manualZoomMultiplicator;
            
            g.drawRect((int)(this.dynamicPos.getAbs()-DX/2),
                    (int)(this.dynamicPos.getOrd()-DY/2), (int)DX, (int)DY);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e)
    {
        
    }
    
    @Override
    public void mousePressed(MouseEvent e)
    {
        double realMultiplicator = (this.main.manualZoomMultiplicator < 1 ?
                1/(-this.main.manualZoomMultiplicator+2)
                : this.main.manualZoomMultiplicator);

        if(e.getButton() == MouseEvent.BUTTON1)
        {
            FractalGenerator.translate(this.main.manGC,
                    e.getX()-this.getWidth()/2+this.main.manGC.getWidth()/2,
                    e.getY()-this.getHeight()/2+this.main.manGC.getHeight()/2);
            this.main.manGC.setZoom(this.main.manGC.getZoom()*realMultiplicator);
            this.main.manImage = FractalGenerator.createMandelbrotFractal(this.main.manGC);
            this.repaint();
        }
        else if(e.getButton() == MouseEvent.BUTTON3)
        {
            FractalGenerator.translate(this.main.manGC,
                    e.getX()-this.getWidth()/2+this.main.manGC.getWidth()/2,
                    e.getY()-this.getHeight()/2+this.main.manGC.getHeight()/2);
            this.main.manGC.setZoom(this.main.manGC.getZoom()/realMultiplicator);
            this.main.manImage = FractalGenerator.createMandelbrotFractal(this.main.manGC);
            this.repaint();
        }
        
        this.main.stateLabel.setText(this.main.information(this.main.manGC));
        System.out.println(this.main.information(this.main.manGC));
    }
    
    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseDragged(MouseEvent e)
    {
        
    }
    
    @Override
    public void mouseMoved(MouseEvent e)
    {
        this.dynamicPos = new Point(e.getX(), e.getY());
        this.repaint();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        if(e.getWheelRotation() > 0)
        {
            this.main.manualZoomMultiplicator--;
            this.main.stateLabel.setText(this.main.information(this.main.manGC));
            this.repaint();
        }
        else if(e.getWheelRotation() < 0)
        {
            this.main.manualZoomMultiplicator++;
            this.main.stateLabel.setText(this.main.information(this.main.manGC));
            this.repaint();
        }
    }
}
