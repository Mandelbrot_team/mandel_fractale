/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fractal.avec.thread;

/**
 *
 * @author Grégoire
 */
public class Mandelbrot {
    public native void  get();
    static {
        System.loadLibrary("mdlbrt");
    }
    public static void main(String[] args) {
        Mandelbrot m = new Mandelbrot();
        m.get();
    }
    
}
